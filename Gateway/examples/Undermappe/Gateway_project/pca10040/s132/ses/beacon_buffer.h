#ifndef BEACON_BUFFER_H
#define BEACON_BUFFER_H

#include "beacon_scanner.h"
#include "utilities.h"
#include <stdint.h>
#include <stdbool.h>


#define BB_SIZE 25
#define BB_INSTANCE_ID_BYTE_SIZE 6



typedef struct
{
  uint8_t beacon_instance_id[BB_INSTANCE_ID_BYTE_SIZE];
  int8_t rssi; 
}bb_data; 

bool bb_is_in_beacon_buffer(uint8_t * instance_id);
bool bb_instance_id_equals(uint8_t * instance_id_1, uint8_t * instance_id_2);

//naiv adding beacon_data to the "beacon -buffer" whitout checking for duplicates of beacons
void bb_add_beacon_data(bb_data new_beacon);

//adding beacon_data if and only if the instance_id, new_beacon not in "beacon-buffer"
void bb_add_beacon_data_as_union(bb_data new_beacon);

//return the number of discovered beacons in "beacon-buffer"
uint16_t bb_get_number_of_beacons();

bb_data * bb_get_beacon(uint16_t index);

//deleting all the beacons in "beacons-buffer"
void bb_clear_beacon_buffer();

//print out the "beacon buffer" to the console
void bb_print();



#endif