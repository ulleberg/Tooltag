#ifndef  GATEWAY_CONFIG_H
#define GATEWAY_CONFIG_H

#include "beacon_buffer.h"

#include "utilities.h"
#include "data_buffer.h"
#include <stdint.h>
#include <stdbool.h>

#define BDATA_GUUID_SIZE 4

#define LAT_SIZE 10
#define LNG_SIZE 11


#define BDATA_LONG_SIZE LNG_SIZE
#define BDATA_LAT_SIZE LAT_SIZE
#define BEACON_UUID_SIZE 12

#define MAXIMUM_BEACON_IN_PACKAGE 4

#endif