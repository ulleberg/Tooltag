#include "data_buffer.h"

#include <stdint.h>
#include <stdbool.h>

char BDATA_GUUID[BDATA_GUUID_SIZE]= GATEWAY_UUID;
uint16_t m_beacon_sent = 0;


void datab_add_beacon_data(bb_data* beacon)
{
  if(BDATA_PACKAGE_SIZE<m_cursor+BB_INSTANCE_ID_BYTE_SIZE+2)
  {
    SEGGER_RTT_printf(0,"overflow in beacon data\r\n");
    return;
  }

  char str[2];
  for(int i=0;i<BB_INSTANCE_ID_BYTE_SIZE;i++)
  {
    
    uint8_to_hex_str(beacon->beacon_instance_id[i],str);
    m_data_buffer[m_cursor++] = str[0];
    
    m_data_buffer[m_cursor++] = str[1];
  }
  
  uint8_t rssi= (uint8_t)(beacon->rssi*-1);
  uint8_to_hex_str(rssi,str);
  m_data_buffer[m_cursor++]=str[0];
  m_data_buffer[m_cursor++]=str[1];
  m_data_buffer[m_cursor++]=';';
}


void datab_clear()
{
 for(int i = BDATA_GUUID_SIZE+1; i<BDATA_PACKAGE_SIZE;i++)
 {
  m_data_buffer[i]=0;
 }
 m_cursor = BDATA_GUUID_SIZE+1;//shuld not erase semicolon
}


void datab_set_end()
{ 
  m_data_buffer[m_cursor++]='\0';
}


char * datab_get_str()
{
  return m_data_buffer;
}


void datab_init()
{
  m_cursor=0;
  
  for(int i = 0; i < BDATA_GUUID_SIZE; i++)
  {
    m_data_buffer[m_cursor++]=BDATA_GUUID[i];
  }
  m_data_buffer[m_cursor++] = ';';
}


void datab_add_gps_lng_lat(char* lng, char* lat)
{
  for(int i = 0; i < BDATA_LONG_SIZE; i++)
  {
    m_data_buffer[m_cursor++]=lng[i];
  }

  m_data_buffer[m_cursor++]=';';

  for(int i = 0; i < BDATA_LAT_SIZE; i++)
  {
    m_data_buffer[m_cursor++]=lat[i];
  }
  m_data_buffer[m_cursor++]=';';
}


void datab_fetch_beacons()
{
  int left_to_send = MIN(bb_get_number_of_beacons()-m_beacon_sent, MAXIMUM_BEACON_IN_PACKAGE  );
  while(left_to_send--)
  {
      datab_add_beacon_data(bb_get_beacon(m_beacon_sent++));
  }
}


void datab_send_package()
{

  SEGGER_RTT_printf(0,"this function is outdated, we are sending the packet from main from main");
  SEGGER_RTT_printf(0, "Package being sent: %s \r\n", datab_get_str());
  datab_clear();
}


bool datab_available_beacons()
{ 
  return m_beacon_sent<bb_get_number_of_beacons();
}


void datab_close()
{
  m_beacon_sent = 0;
}
