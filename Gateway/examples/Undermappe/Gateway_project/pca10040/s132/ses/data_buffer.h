#ifndef DATA_BUFFER_H
#define DATA_BUFFER_H

#include "beacon_scanner.h"
#include "beacon_buffer.h"
#include "gateway_config.h"
#include "gateway_unique_config.h"
#include "utilities.h"

#include "SEGGER_RTT.h"
#include <stdint.h>
#include <stdbool.h>


#define BDATA_PACKAGE_SIZE 500


static uint16_t m_cursor = 0;
static char m_data_buffer[BDATA_PACKAGE_SIZE] = {0};

char * datab_get_str();

void datab_add_beacon_data(bb_data* beacon);
void datab_add_gps_lng_lat(char * lng, char *lat);
void datab_clear();
void datab_set_end();
void datab_init();

void datab_fetch_beacons();
void datab_send_package();
bool datab_available_beacons();
void datab_close();


#endif