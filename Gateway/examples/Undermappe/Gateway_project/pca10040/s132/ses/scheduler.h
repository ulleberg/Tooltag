#ifndef SCHEDULER_H
#define SCHEDULER_H


#include "app_scheduler.h"
#include "nordic_common.h"

#include "app_timer.h"
// Scheduler settings

#define SCHED_QUEUE_SIZE                10                               /**< Maximum number of events in the scheduler queue. */
#define SCHED_MAX_EVENT_DATA_SIZE       APP_TIMER_SCHED_EVENT_DATA_SIZE 

#endif