#include "utilities.h"
#include <stdint.h>
#include <stdbool.h>


#include "SEGGER_RTT.h"

void print_hex(uint8_t d)
{
 

  if(d<0x10)
  {
    SEGGER_RTT_printf(0,"0%x",d);
  }
  else
  {
    SEGGER_RTT_printf(0,"%x",d);
  }
  
}

void print_hex_a(uint8_t* array, uint16_t length)
{
  for(int i = 0; i <length; i++)
  {
    print_hex(array[i]);
    SEGGER_RTT_printf(0, " ");
  }
}


void deep_copy_uint8_t_array(uint8_t* array, uint8_t* copy, uint16_t length)
{
  for(int i = 0; i<length; i++)
  {
    copy[i] = array[i];
  }
}

static char m_hex[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
void uint8_to_hex_str(uint8_t x, char* str)
{
  char xl  = x>>4;
  str[0] = m_hex[xl];
  char xr = (x & 0x0F);
  str[1] = m_hex[xr];
}
