#include "beacon_buffer.h"

#include "SEGGER_RTT.h"


static bb_data m_beacon_buffer[BB_SIZE]; 
static uint16_t m_number_of_beacons = 0;

bool bb_instance_id_equals(uint8_t * instance_id_1, uint8_t * instance_id_2)
{
  for(int i = 0; i<BB_INSTANCE_ID_BYTE_SIZE; i++)
  {
    if(instance_id_1[i] != instance_id_2[i])
    {
      return false;
    }
  }
  return true;
}


void bb_add_beacon_data(bb_data new_beacon)
{
  if(m_number_of_beacons>=BB_SIZE)
  {
    return;
  }
  m_beacon_buffer[m_number_of_beacons++] = new_beacon;
}


void bb_add_beacon_data_as_union(bb_data new_beacon)
{
  if(m_number_of_beacons>=BB_SIZE)
  {
    SEGGER_RTT_printf(0,"It apperce that it to many beacons nearby.\r\n");
    return;
  }

  if(bb_is_in_beacon_buffer(new_beacon.beacon_instance_id))
  {
    //DO NOTHiNG
  }
  else
  {
    m_beacon_buffer[m_number_of_beacons++] = new_beacon;
  }
}


bool bb_is_in_beacon_buffer(uint8_t * instance_id)
{
  for(int i = 0; i<m_number_of_beacons;i++)
  {
    if(bb_instance_id_equals(m_beacon_buffer[i].beacon_instance_id, instance_id))
    {
      return  true;
    }
  }
  return false;
}


uint16_t bb_get_number_of_beacons()
{
  return m_number_of_beacons;
}


void bb_clear_beacon_buffer()
{
  m_number_of_beacons = 0;
}


void bb_print()
{
  for(int i = 0; i<m_number_of_beacons; i++)
  {
    SEGGER_RTT_printf(0,"ID: ");
    print_hex_a(m_beacon_buffer[i].beacon_instance_id, BB_INSTANCE_ID_BYTE_SIZE);
    SEGGER_RTT_printf(0,", RSSI: %d\r\n",m_beacon_buffer[i].rssi);
  }
}


bb_data * bb_get_beacon(uint16_t index)
{
    return &(m_beacon_buffer[index]);
}

