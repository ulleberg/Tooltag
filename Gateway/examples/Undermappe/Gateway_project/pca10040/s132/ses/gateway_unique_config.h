#ifndef GATEWAY_UNIQUE_CONFIG_H
#define GATEWAY_UNIQUE_CONFIG_H
/*
THESE DEF SHOULD/MUST BE UNIQUE FOR EVERY GATEWAY/PCB/CHIP
*/
#define IMEI 357517080119474//this can be verified by checking the number writen on the sara-module
#define GATEWAY_UUID {'A','B','C','D'}  //ID for Byggmesteran test: ABBA, FAKTRY: ABCD

//faking the data since the gps is too slow for a demo
#define LONGITUDE_DUMMY {'0', '1', '0', '2', '4', '.', '1', '3', '2', '9', '0'}
#define LATITUDE_DUMMY {'6', '3', '2', '3', '.', '8', '0', '8', '5', '4'} 

//Byggmesteran, Verdal coordinates: 6347.16126, N, 01127.33030, E
//FAKTRY, Sluppen coordinates: 6323.80854, N, 01024.13290, E

#endif