#ifndef BEACON_SCANNER_H
#define BEACON_SCANNER_H



#include "beacon_buffer.h"

void bc_init(void);
void bc_scan_start(void);
void bc_scan_stop(void);


#endif