
#ifndef UTILITIES_H
#define UTILITIES_H


#include "app_timer.h"
#include "app_util.h"
#include "bsp_btn_ble.h"
#include "ble.h"
#include "ble_gap.h"
#include "ble_hci.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"


//TODO make deep_copy_more_genreal
//#define deep_copy_array(array) TRY THIS LATER...

void print_hex(uint8_t d);
void print_hex_a(uint8_t* array, uint16_t length);
void deep_copy_uint8_t_array(uint8_t* array, uint8_t* copy, uint16_t length);

#endif


























