#ifndef GNSS_H
#define GNSS_H


#include "nrf_drv_spi.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "app_timer.h"
#include "app_error.h"
#include <string.h>
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "gateway_config.h"
#include "gateway_unique_config.h"
#include "SEGGER_RTT.h"
#include "sdk_config.h"

#define TIMES_UP     60                    /* Time the GNSS will run in seconds */
#define UPDATE_SEC   30

#define TRANSFER_LENGTH 250                 /* Lengt of the transfer buffer */


uint32_t seconds_since_start = 0;           /* Used in GNSS_run*/
uint32_t seconds_since_last_update = 0;     /* Used in GNSS_run*/


#define SPI_INSTANCE  0                                                 /**< SPI instance index. */
static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);    /**< SPI instance. */
static volatile bool spi_xfer_done;                                     /**< Flag used to indicate that SPI instance completed the transfer. */


static uint8_t  transfer_buf[TRANSFER_LENGTH]={0};                      /**< TX buffer. */
static uint8_t  receive_buf[sizeof(transfer_buf)];                      /**< RX buffer. */
static const uint8_t m_length = sizeof(transfer_buf);                   /**< Transfer length. */


uint8_t latitude[LAT_SIZE]={0};                                         /** Used to store and send out data for latitude*/
uint8_t longitude[LNG_SIZE]={0};                                        /** Used to store and send out data for longetude*/

uint8_t test_latitude[LAT_SIZE] =  LATITUDE_DUMMY;                      /* DUMMY COORDINATES to the demo we had */
uint8_t test_longitude[LNG_SIZE] = LONGITUDE_DUMMY;                     /* DUMMY COORDINATES to the demo we had */

uint8_t *lat = &test_latitude[0];
uint8_t *lng = &test_longitude[0];


#define RMC_string_pringt                                             /*To print the hole RMC streng for test*/
#ifdef RMC_string_pringt
char PosToPrint[TRANSFER_LENGTH];
#endif 

bool Check_If_Latitude_Is_Correct(void);  

bool Check_If_Longitude_Is_Correct(void);

void GNSS_Read(void);

void GNSS_Sort_Out_Coordinates(void);

void spi_event_handler(nrf_drv_spi_evt_t const * p_event,
                       void *                    p_context);


void Spi_init(void);

void GNSS_sleep(void);

void run_GNSS(void);

uint8_t * GNSS_get_lat_p(void);
uint8_t * GNSS_get_lng_p(void);

bool GNSS_has_fix(void);

#endif