
#ifndef SARA_NBIOT_H
#define SARA_NBIOT_H

#include <stdbool.h>
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_serial.h"
#include "nrf_drv_power.h"
#include "nrfx_power.h"
#include "nrf_queue.h"
#include "gateway_unique_config.h"
#include <string.h>

#define SARA_RESET 11
#define STALKIT_LED 6

#define OP_QUEUES_SIZE 3
#define APP_TIMER_PRESCALER NRF_SERIAL_APP_TIMER_PRESCALER
#define RX_PIN_NUM 5
#define TX_PIN_NUM 4

#define SERIAL_FIFO_TX_SIZE 256
#define SERIAL_FIFO_RX_SIZE 512

#define CMD_BUFF_TX_LEN 256
#define CMD_BUFF_RX_LEN 512

#define SERIAL_BUFF_TX_SIZE 1
#define SERIAL_BUFF_RX_SIZE 1

#define PREFIX "\r\nAT+"
#define POSTFIX "\r\n"

#define REMOTE_IP "54.77.39.136"
#define REMOTE_PORT 31415
//#define REMOTE_IP "2.150.48.180"
//#define REMOTE_PORT 11032
#define LOCAL_PORT 8000
static int m_socket;

static char m_cmd_buffer_tx[CMD_BUFF_TX_LEN];
static char m_cmd_buffer_rx[CMD_BUFF_RX_LEN];

//static uint32_t my_timer_seconds;
//#define MY_TIMER NRF_TIMER1
//#define MY_TIMER_IRQn TIMER1_IRQn
//#define MY_TIMER_IRQHandler TIMER1_IRQHandler

// ----- init -------
void sara_init(void);
static void m_uart_init(void);

// ----- Sleep handler -------
static void m_sleep_handler(void);

// ----- Radio connection -------
bool sara_connect(void);
bool sara_connected(void);
void sara_creat_socket(void);
void sara_close_socket(void);

bool sara_has_socket(void);//made by volrath ready



// ----- write/send to module -------
static void m_write_at_cmd(void);
void sara_send_cmd(const char *msg);
void sara_send_data(const char *data);

// ----- read from module -------
static void m_sara_read(void);

// ----- respons handle -------
static bool m_sara_ok_response(void);
const char *p_get_response(void);

// ----- cmd manipulation -------
static void m_creat_at_cmd(const char *msg);
void sara_modem_info(void);
static void m_creat_data_at_cmd(const char *data);
static void add_header();


void sara_end(void);

// ----- Timer -------
static void my_timer_start(void);
static uint32_t my_timer_get_ms(void);
static uint64_t my_timer_get_us(void);
void MY_TIMER_IRQHandler(void);

// ------- Not in use -------------
void sara_debug();

#define WRITE_CMD(msg)                                              \
  do                                                                \
  {                                                                 \
    m_write_at_cmd(PREFIX msg POSTFIX, sizeof(PREFIX msg POSTFIX)); \
  } while (0)

#endif // SARA_NBIOT_H