/**
 * Copyright (c) 2016 - 2018, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */


#include "utilities.h"
#include "beacon_buffer.h"
#include "beacon_scanner.h"   
#include "init.h"
#include "GNSS.h"
#include "timer.h"
#include "scheduler.h"
#include "data_buffer.h"
#include "sara_nbiot.h"


#define MS_PER_MIN 60000
#define BEACON_SCANNING_TIME 10000                                   /* How many milliseconds the beaconscanner will scan, before being stopped */
#define TIME_INTERVALL_GATEWAY 1.5*MS_PER_MIN                        /* How long the gateways interval/cycle will last. 
                                                                        The cycle determends how frequent the gateway will scanning for GPS signals, beacons signals, 
                                                                        and send data packets */


#define GATEWAY_INTERVAL APP_TIMER_TICKS(TIME_INTERVALL_GATEWAY)      /* Makes milliseconds into tickes, that is needed for the oscillator that app_timer uses */

APP_TIMER_DEF(m_my_timer_id);                                         /* Making a timer*/

static volatile uint8_t state=0;                                      /* Used to start the process of scanning and sending the data*/




/**@snippet [Handling events from the ble_nus_c module]
 @brief Function for shutdown events.*/

static bool shutdown_handler(nrf_pwr_mgmt_evt_t event)
{
    ret_code_t err_code;

    err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    switch (event)
    {
        case NRF_PWR_MGMT_EVT_PREPARE_WAKEUP:
            // Prepare wakeup buttons.
            err_code = bsp_btn_ble_sleep_mode_prepare();
            APP_ERROR_CHECK(err_code);
            break;

        default:
            break;
    }
    return true;
}


NRF_PWR_MGMT_HANDLER_REGISTER(shutdown_handler, APP_SHUTDOWN_HANDLER_PRIORITY); 


/**@brief Function for handling the idle state (main loop).
 * @details Handle any pending log operation(s), then sleep until the next event occurs.
 */
static void idle_state_handle(void)
{
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}


/* Function for updating the list over beacons. Does the following things;
  1. Start scanning
  2. Scan for a certain time
  3. Stop scanning 
*/
void BLE_update(void)
{
      bc_scan_start();
      nrf_delay_ms(BEACON_SCANNING_TIME);   /* To not stop the scanning before the scanningtime is over*/
      bc_scan_stop();
      
      nrf_delay_ms(1000);

      SEGGER_RTT_printf(0,"Num of beacon %d \n \r",bb_get_number_of_beacons());   //For debug purposes
      nrf_delay_ms(500);
 
      bb_print(); //For debug purposes
      nrf_delay_ms(1000);
}


/* Makes a packet that the data can be stored in and sent to the server*/
static void databuffer_create(void)                  
{
    datab_init();
}


/* Function that adds the GPS and becon data, and then sends it */
static void databuffer_send_all(void)                 
{
  while(datab_available_beacons())
  {
    datab_add_gps_lng_lat(lng, lat);
    datab_fetch_beacons();
    datab_send_package();   
  }
  datab_close();
}


/* Handler for when the timer cycletime for the gateway runs ut */
static void my_timeout_handler(void * p_context)
{
    UNUSED_PARAMETER(p_context);
    state = 0;
}


/* Function for initializing and start the timer module.*/
static void timer_start(void)                             
{
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);


    err_code = app_timer_create(&m_my_timer_id,
                                APP_TIMER_MODE_REPEATED,       /* The timer will restart each time it expires*/
                                my_timeout_handler);           /* timeout_handler;     Function to be executed when the timer expires*/
    APP_ERROR_CHECK(err_code);

    err_code = app_timer_start(m_my_timer_id,
                                GATEWAY_INTERVAL,             /* Time is given in millisekund*/
                                NULL); 
    APP_ERROR_CHECK(err_code);
}



int main(void)
{
    bsp_board_init(BSP_INIT_LEDS);          /* If you want to debug using the led on stalkIT*/
   
    log_init();                             /* Initiating the log, used for debug purposes*/
    
    Spi_init();                             /* Initiating the spi, and makes sure that the GNSS-modul is in sleep */
     
    timer_start();                          /* Creates and start a timer called my_timer*/

    sara_init();                            /* Initiating SARA, used to send data over NB-IoT */
        
    bc_init();                              /* bc_init initializes BLE, UART and more*/

    sara_connect();                         

    databuffer_create();                    /* Create a databuffer that is send over the NB-Iot network*/

    power_management_init();                /* Function for initializing power management.*/
    
    nrf_delay_ms(5000);

    while(true)
    { 
      
        idle_state_handle();         /* Includes nrf_pwr_mgmt_run();
                                        Function for running power management. Should run in the main loop. */  
        if(state==0)
        {          
            run_GNSS();              /* Wake up GNSS, looking for gps coordinate fix, runs for a maximum time defined in GNSS.h */

            BLE_update();            /* Bluetooth module update*/
                     
            SEGGER_RTT_printf(0, "has fix: %d\r\n",GNSS_has_fix() );   //For debug purposes

            if(GNSS_has_fix()){
                lat = GNSS_get_lat_p();
                lng = GNSS_get_lng_p();
            }


            bool first_time_in_beacon_fetcher_loop = true;    /* Used to send position when no beacons are detected*/

           /* This loop is responsible for collecting and sending data and will enter if the system finds any nearby beacons.
              The second argument will make sure the loop enters even if there are no beacons around such that it at least sends its position. */
            while(datab_available_beacons() || first_time_in_beacon_fetcher_loop)
            {
                datab_add_gps_lng_lat(lng, lat); 
                first_time_in_beacon_fetcher_loop = false;
              
            
                datab_fetch_beacons();            
             
                if(!sara_connected())
                {
                  sara_connect();   /* Try to connect again if connection was lost since last update */
                }

                
                /*If SARA is connected it creates a socket for the sara module and send the package */
                if(sara_connected())         
                {
                  sara_creat_socket();           
                  nrf_delay_ms(1000);

                  if(sara_has_socket())
                  { 
                    SEGGER_RTT_printf(0, "Data package: %s\r\n", datab_get_str());  // For debug purposes
                    sara_send_data(datab_get_str());
                    nrf_delay_ms(500);
                    sara_close_socket();
                  }

                }
                else
                {
                  SEGGER_RTT_WriteString(0, "Lost connection...\r\n");  // For debug purposes
                }
                
                nrf_delay_ms(1000);
                datab_clear();           /*Deleting all data from the databuffer except the gateway ID */                                 
            }
            
          datab_close();
          bb_clear_beacon_buffer();      /*To clean the list over beacon after they are sent*/
            
          state = 1;
          SEGGER_RTT_printf(0, "Going into Low Power Mode.\r\n"); // For debug purposes
        }
    }
}


