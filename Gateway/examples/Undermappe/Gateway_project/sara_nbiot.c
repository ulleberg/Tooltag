#include "sara_nbiot.h"
#include "SEGGER_RTT.h" //---------- DEBUG -----------

NRF_SERIAL_DRV_UART_CONFIG_DEF(m_uart0_drv_config,
                               RX_PIN_NUM, TX_PIN_NUM,
                               0xFF, 0xFF,
                               NRF_UART_HWFC_DISABLED, NRF_UART_PARITY_EXCLUDED,
                               NRF_UART_BAUDRATE_9600,
                               UART_DEFAULT_CONFIG_IRQ_PRIORITY);

NRF_SERIAL_QUEUES_DEF(serial_queues, SERIAL_FIFO_TX_SIZE, SERIAL_FIFO_RX_SIZE);

NRF_SERIAL_BUFFERS_DEF(serial_buffs, SERIAL_BUFF_TX_SIZE, SERIAL_BUFF_RX_SIZE);

NRF_SERIAL_CONFIG_DEF(serial_config, NRF_SERIAL_MODE_IRQ,
                      &serial_queues, &serial_buffs, NULL, NULL);

NRF_SERIAL_UART_DEF(serial_uart, 0); /*!<Endra fra 0 til 1 ID til ny timer*/

// ********************************** init **********************************

bool sara_has_socket(void)//made by volrath ready
{
  return m_socket != -1;
}


void sara_init(void)
{
    SEGGER_RTT_WriteString(0, "Initialisation SARA module...\r\n");   //for debug purposes

    ret_code_t ret;
    /*
    // --------- Is this necessary? ---------
    nrf_gpio_cfg_output(TX_PIN_NUM);
    nrf_gpio_pin_write(TX_PIN_NUM, 1);
    nrf_gpio_cfg_output(RX_PIN_NUM);
    nrf_gpio_pin_write(RX_PIN_NUM, 1);
    // --------------------------------------
    */
    m_socket = -1;
    nrf_gpio_cfg_output(STALKIT_LED);
    nrf_gpio_pin_write(STALKIT_LED, 1);

/*!<CLOCK ALREADY INITIALIZED IN MAIN WITH TIMER_INIT()*/
    ret = nrf_drv_clock_init();
    APP_ERROR_CHECK(ret);
    nrf_delay_ms(100);

    ret = nrf_drv_power_init(NULL);
    APP_ERROR_CHECK(ret);

    nrf_drv_clock_lfclk_request(NULL);

    m_uart_init();

    nrf_gpio_pin_write(STALKIT_LED, 0);
}

static void m_uart_init(void)
{
    ret_code_t ret = nrf_serial_init(&serial_uart, &m_uart0_drv_config, &serial_config);
    APP_ERROR_CHECK(ret);
}

// ********************************** Sleep handler **********************************
static void m_sleep_handler(void)
{
    __WFE();
    __SEV();
    __WFE();
}

// ********************************** Radio connection **********************************
// --------- sara_connect()---------
// Need to run until sara_connected gets true,
// if not true after 20 sec, run a reboot.
// If sara_connected stays false for ever,
// break and return false.
//
// May need to verify that NCONFIG is correct (
// SCRAMBLING=true, AVOID=true and AUTOCONNECT=true).
// CFUN=1 and CGATT=1 seems also unnecessary
// ---------------------------------
bool sara_connect(void)
{
     SEGGER_RTT_WriteString(0, "Connecting to SARA module...\r\n");   //for debug purposes
    uint8_t number_of_reboots = 0;
    //my_timer_start();
    nrf_delay_ms(2000);
    sara_send_cmd("CFUN=1"); // radio on
    sara_send_cmd("CGATT=1"); // connect to basestation
    
    uint32_t seconds_since_start = app_timer_cnt_get()/8;

    while (!sara_connected()){
        SEGGER_RTT_WriteString(0, "Failed to connect!\r\nAttempting to connect...");
        if (((app_timer_cnt_get()/8)-seconds_since_start > 20))
         // May take up to 15 sec(20 to be sure) to connect up, if it does not connect, you must perform a reboot
        {
            
            if (number_of_reboots > 5)
            {
                sara_send_cmd("NCONFIG=\"AUTOCONNECT\",\"TRUE\"");
                sara_send_cmd("NCONFIG=\"CR_0354_0338_SCRAMBLING\",\"TRUE\"");
                sara_send_cmd("NCONFIG=\"CR_0859_SI_AVOID\",\"TRUE\"");
                number_of_reboots = 0;
            }
            seconds_since_start = app_timer_cnt_get()/8;
            sara_send_cmd("NRB"); // reboot
            SEGGER_RTT_WriteString(0, ".");
            number_of_reboots++; // counter
        }

    }
    //SEGGER_RTT_printf(0, "Connection time: %d \r\n", my_timer_get_ms()); //---------- DEBUG -----------
    
    SEGGER_RTT_WriteString(0, "Connected!\r\n");
    return true;
}

// --------- sara_connected()---------
//
// -----------------------------------
bool sara_connected(void)
{
    sara_send_cmd("CGATT?");
    if (m_sara_ok_response())
    {
        const char *resp = p_get_response();
        if (resp[10] == '1')
        {
            nrf_gpio_pin_write(STALKIT_LED, 1);
            return true;
        }
    }
    return false;
}

// --------- sara_creat_socket()---------
//
// -----------------------------------
void sara_creat_socket(void)
{   
    SEGGER_RTT_WriteString(0, "Creating socket...\r\n");
    int counter = 0;
    m_socket =-1;//volrath vi lager ny socket
    while(m_socket != 0 || (counter++) < 7) //alle gode ting er 3, vi pr�ver tre ganger :)
    {
        char cmd[30] = {0};
        sprintf(cmd, "NSOCR=\"DGRAM\",17,%d,1", LOCAL_PORT);
        //SEGGER_RTT_printf(0, "msg: %s\r\n", cmd); //---------- DEBUG -----------
        sara_send_cmd(cmd);
        const char *resp = p_get_response();
        m_socket = atoi(resp[2]);
    }
}

// --------- sara_close_socket()---------
// Run this function before you make a socket!!!
// Closing one socket!
// If you makes two sockets you need to write NSOCL=1,
// tree sockets NSOCL=2 and so on...
// -----------------------------------
void sara_close_socket(void)
{
    
    SEGGER_RTT_WriteString(0, "Attempting to close socket... ");
    char *msg = "NSOCL=0";
    // char msg[30] = {0};
    // strcat(msg, "NSOCL=");
    // strcat(msg, socket);
    sara_send_cmd(msg);
    if(m_sara_ok_response())
    {
          m_socket = -1;
        SEGGER_RTT_WriteString(0, "Socket closed! \r\n");
    }
    else{
        SEGGER_RTT_WriteString(0, "Failed to close socket! \r\n");
    }
}

// ********************************** Write/Send to module **********************************
static void m_write_at_cmd()
{
   ret_code_t ret = nrf_serial_write(&serial_uart,
                                      m_cmd_buffer_tx,
                                      strlen(m_cmd_buffer_tx), // Exclude NULL, (strlen -1)?
                                      NULL,
                                      2000);

    // SEGGER_RTT_printf(0, "write ret: %d\r\n", ret); //---------- DEBUG -----------
    APP_ERROR_CHECK(ret);
}

// --------- sara_send_cmd---------
// Running necessary functions to perform
// a proper write and receive cycle of AT commands.
// --------------------------------
void sara_send_cmd(const char *msg)
{
    m_creat_at_cmd(msg);
    m_write_at_cmd();
    m_sara_read();
    if(!m_sara_ok_response()){
        //SEGGER_RTT_printf(0, "ERROR respons cmd: %s\r\nmsg: %s\r\nsocket: %d\r\n", p_get_response(), m_cmd_buffer_tx, m_socket); //---------- DEBUG -----------
    }
}

// TODO Not done
void sara_send_data(const char *data)
{
    
    //SEGGER_RTT_printf(0, "m_socket before writing AT command: %d\r\n", m_socket);
    m_creat_data_at_cmd(data);
    SEGGER_RTT_WriteString(0, "Entering m_write_at_cmd...\r\n");
    m_write_at_cmd();
    m_sara_read();
    if(!m_sara_ok_response()){
        SEGGER_RTT_printf(0, "ERROR respons data: %s\r\nmsg: %s\r\nsocket: %d\r\n", p_get_response(), m_cmd_buffer_tx, m_socket); //---------- DEBUG -----------
    }
    
    SEGGER_RTT_WriteString(0, "Data attempted sent, result unknown\r\n");//TODO find away to check if data is sent in runtime
}

// ********************************** Read from module **********************************
// --------- m_sara_read()---------
// This function runes every time after
// an AT-command is written to the module
// It stores the received data in m_cmd_buffer_rx.
// You can get this data with p_get_response() func.
//
// TODO understand why nrf_serial_read()
// always return an error, and then handle the error.
// --------------------------------
static void m_sara_read(void)
{
    memset(m_cmd_buffer_rx, 0x00, sizeof(m_cmd_buffer_rx));
    ret_code_t ret = nrf_serial_read(&serial_uart, m_cmd_buffer_rx, sizeof(m_cmd_buffer_rx), NULL, 1000);
    
    // If m_cmd_buffer_rx == 0 it means that there has been a reset on the Sara module,
    // this gives a frame error which makes the serial_read not working.

    if (strlen(m_cmd_buffer_rx) == 0)
    {
        SEGGER_RTT_WriteString(0, "Failed to read (Check connection to Sara), trying again...\r\n");
        nrf_serial_uninit(&serial_uart);
        m_uart_init();
        nrf_delay_ms(3000); // Takes some time before the module is ready
        nrf_serial_tx_abort(&serial_uart);
        m_write_at_cmd(); // Writes the last AT command once more to be sure it has been sent.
        m_sara_read();
    }
}

// ***************************** Respons handling *****************************
// --------- m_sara_ok_response---------
// All respons ends with OK\r\n\0 or ERROR\r\n\0
// Therefor checking len-4 and len-3
// -------------------------------------
static bool m_sara_ok_response(void)
{
    return (m_cmd_buffer_rx[strlen(m_cmd_buffer_rx) - 4] == 'O' && m_cmd_buffer_rx[strlen(m_cmd_buffer_rx) - 3] == 'K');
}

// --------- p_get_response---------
// returns just the rx buffer
// --------------------------------
const char *p_get_response(void)
{
    return m_cmd_buffer_rx;
}

// ********************************** Cmd manipulation **********************************
static void m_creat_at_cmd(const char *msg)
{
    sprintf(m_cmd_buffer_tx, "%s%s%s\0", PREFIX, msg, POSTFIX);
    //---------- What is the best approach here, above or below? ---------
    //    strncpy(m_cmd_buffer_tx, PREFIX, strlen(PREFIX));
    //    strcat(m_cmd_buffer_tx, msg);
    //    strcat(m_cmd_buffer_tx, POSTFIX);
    //    DEBUG("m_creat_at_cmd cmd: %s\n", m_cmd_buffer_tx);
    //    DEBUG("m_creat_at_cmd cmd len: %d\n", strlen(cmd));
    //    strncpy(m_cmd_buffer_tx, cmd, strlen(m_cmd_buffer_tx));
    //    m_cmd_buffer_tx[strlen(m_cmd_buffer_tx) -1] = 0;
    //--------------------------------------------------------------------
}


// --------- m_creat_data_at_cmd()---------
// This func creats a at command with the data you want to send
//
// TODO need to understand how to generate this number: 01014528EE8EB5BD00DC1CBDB5C908.
// This number is generated from massage_type = 1, m_imei = 357517080049085 and m_imsi = 242016000002312
// --------------------------------
static void m_creat_data_at_cmd(const char *data)
{
    nrf_delay_ms(500);
    char buffer[256] = {0};
    unsigned long long tmp = IMEI;
    buffer[0] = 1;
    for (int i = 0; i < 7; i++)
    {
        buffer[7 - i] = (char)(tmp & 0xFF);
        tmp >>= 8;
    }
    tmp = 242016000002312;
    for (int i = 0; i < 7; i++)
    {
        buffer[14 - i] = (char)(tmp & 0xFF);
        tmp >>= 8;
    }

    char temp[30];

    for (int i = 0; i < 15; i++)
    {
        unsigned char ch1 = (buffer[i] & 0xF0) >> 4;
        unsigned char ch2 = (buffer[i] & 0x0F);
        if (ch1 <= 9)
        {
            ch1 = (char)('0' + ch1);
        }
        else
        {
            ch1 = (char)('A' + ch1 - 10);
        }
        if (ch2 <= 9)
        {
            ch2 = (char)('0' + ch2);
        }
        else
        {
            ch2 = (char)('A' + ch2 - 10);
        }
        temp[2*i] = ch1;
        temp[2*i+1] = ch2;
    }

    
    sprintf(m_cmd_buffer_tx, "%sNSOST=%d,\"%s\",%d,%d,\"", PREFIX, m_socket, REMOTE_IP, REMOTE_PORT, strlen(data) + 15); 

    
   sprintf(m_cmd_buffer_tx + strlen(m_cmd_buffer_tx), "%s", temp);
   SEGGER_RTT_printf(0, "m_socket before writing AT command: %d\r\n", m_socket);
    
    for(int i = 0; i<strlen(data); i++){
       sprintf(m_cmd_buffer_tx + strlen(m_cmd_buffer_tx), "%02x" , data[i]);
    }

    sprintf(m_cmd_buffer_tx + strlen(m_cmd_buffer_tx), "\"%s", POSTFIX);
}


static void add_header(){

}

void sara_modem_info(void)
{
    sara_send_cmd("UGPIOC?");  // Check gpio config
    sara_send_cmd("CGMR");     // Check firmware version
    sara_send_cmd("CIMI");     // Check IMEI
    sara_send_cmd("NCONFIG?"); // Check Configuration
    sara_send_cmd("NUESTATS"); // Check radio status
}

// ********************************** End handling **********************************
void sara_end(void)
{
    nrf_serial_uninit(&serial_uart);
    SEGGER_RTT_WriteString(0, "----------------- END DEBUG -----------\r\n"); //---------- DEBUG -----------
    SEGGER_RTT_WriteString(0, "\r\n");                                        //---------- DEBUG -----------
}



void sara_debug(){
    SEGGER_RTT_printf(0, "socket after: %s\r\n", p_get_response()); //---------- DEBUG -----------
}

// ********************************** TIMER **********************************
// Code from: https://devzone.nordicsemi.com/f/nordic-q-a/21402/how-to-call-time-function-in-nordic-nrf518222-milliseconds/83887#83887

/*static void my_timer_start(void)
{
    // Reset the second variable
    my_timer_seconds = 0;

    // Ensure the timer uses 24-bit bitmode or higher
    MY_TIMER->BITMODE = TIMER_BITMODE_BITMODE_24Bit << TIMER_BITMODE_BITMODE_Pos;

    // Set the prescaler to 4, for a timer interval of 1 us (16M / 2^4)
    MY_TIMER->PRESCALER = 4;

    // Set the CC[0] register to hit after 1 second
    MY_TIMER->CC[0] = 1000000;

    // Make sure the timer clears after reaching CC[0]
    MY_TIMER->SHORTS = TIMER_SHORTS_COMPARE0_CLEAR_Msk;

    // Trigger the interrupt when reaching CC[0]
    MY_TIMER->INTENSET = TIMER_INTENSET_COMPARE0_Msk;

    // Set a low IRQ priority and enable interrupts for the timer module
    NVIC_SetPriority(MY_TIMER_IRQn, 7);
    NVIC_EnableIRQ(MY_TIMER_IRQn);

    // Clear and start the timer
    MY_TIMER->TASKS_CLEAR = 1;
    MY_TIMER->TASKS_START = 1;
}

static uint32_t my_timer_get_ms(void)
{
    // Store the current value of the timer in the CC[1] register, by triggering the capture task
    MY_TIMER->TASKS_CAPTURE[1] = 1;

    // Combine the state of the second variable with the current timer state, and return the result
    return (my_timer_seconds * 1000) + (MY_TIMER->CC[1] / 1000);
}

static uint64_t my_timer_get_us(void)
{
    // Store the current value of the timer in the CC[1] register, by triggering the capture task
    MY_TIMER->TASKS_CAPTURE[1] = 1;

    // Combine the state of the second variable with the current timer state, and return the result
    return (uint64_t)my_timer_seconds * 1000000 + MY_TIMER->CC[1];
}

// Timer interrupt handler
void MY_TIMER_IRQHandler(void)
{
    if (MY_TIMER->EVENTS_COMPARE[0])
    {
        MY_TIMER->EVENTS_COMPARE[0] = 0;

        // Increment the second variable
        my_timer_seconds++;
    }
}
*/

// **********************************************************************