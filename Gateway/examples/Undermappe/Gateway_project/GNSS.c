#include "GNSS.h"

bool fix_flag= false;               /*Flag used to ceep track if the gps have gotton a signal*/
bool times_up= false;               /*Set to true of the timer when times up in the GNSS_run function*/


bool Check_If_Latitude_Is_Correct(void){
  uint8_t num=0; 
  for(uint8_t i=0; i<sizeof(latitude);i++){

      if(latitude[i]>=48 && latitude[i]<=57){   /* If its a number (ascii= 0-9) */
      num++;
      }
  }
  if(num==9){ /* Should be 9 numbers in a correct latitude data*/ 
    return true; 
  }

  else{
    return false; 
  }
}

/*Look at the comments in function above */
bool Check_If_Longitude_Is_Correct(void){ 
  uint8_t num=0; 
  for(uint8_t i=0; i<sizeof(longitude);i++){
      if(longitude[i]>=48 && longitude[i]<=57){
      num++;
      }
  }
  if(num==10){
    return true; 
    }
  else{
    return false; 
    }
}


void GNSS_Read(void){

    nrf_gpio_pin_clear(GNSS_CS_PIN);                                                                  /* Sets CS to low (CS is activ low) */

    memset(receive_buf, 0, m_length);                                                                 /* Reset rx buffer and transfer done flag */

    ret_code_t err_code=nrf_drv_spi_transfer(&spi, transfer_buf, m_length, receive_buf, m_length);    /* Tansfer to and from master*/

    APP_ERROR_CHECK(err_code);  

    NRF_LOG_FLUSH();                                                                                  /* Emty the log*/  

    nrf_gpio_pin_set(GNSS_CS_PIN);                                                                    /* Set high; deactivate*/
}


void GNSS_Sort_Out_Coordinates(void){

    char *RMC_pos;                                                  /* Pointer to the first finding av $GNRMC */  
    uint16_t CommaCounter = 0;                                      /* Used to count comma in the NMEA-message */
    uint16_t string_length = 0;                                     /* To find the lengt off the info to RMC  */
    uint32_t j=0;                                                   /* Used as a counter */
  

    RMC_pos= strstr(receive_buf, "$GNRMC");                                 /* Gives a pointer to the first found, and if not found returns a zeropointer*/
  

    if(RMC_pos){
      uint32_t pos = (uint32_t) RMC_pos - (uint32_t) receive_buf;           /* Pos will give the position to "$GNRMC" in the receive_buf-vector */
    
      while( CommaCounter <= 10 ){                                          /* Count comma until the info needed from the RMC-message is sorted out */

          if((const char)receive_buf[pos+string_length]==','){              /* New comma = new info space in the GNRMC-message */
          CommaCounter++;
          j=0;                                                              /* Ceeps count on the number of characters after comma */
          }

          else if (CommaCounter==3){                                         /* Latitude is found after 3 commas in the $GNRMC message */ 
          latitude[j]=receive_buf[pos+string_length];
          j++;
          }

          else if (CommaCounter==5){                                         /*Longitude is found after 5 commas in the $GNRMC message */  
          longitude[j]=receive_buf[pos+string_length];
          j++;
          }
               
          string_length++; 
        }
      

      /*To print the RMC-message*/
      #ifdef RMC_string_pringt                      
      for(uint8_t a=0; a<string_length;a++)
      {
          PosToPrint[a]=receive_buf[pos+a];
      }

      SEGGER_RTT_printf(0, "%s \r\n",PosToPrint);
      #endif 
     
    }
}




void Spi_init(void){

    nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
    spi_config.ss_pin   = NRF_DRV_SPI_PIN_NOT_USED;                         /* Was used in the SPI example */            
    spi_config.miso_pin = GNSS_MISO_PIN;
    spi_config.mosi_pin = GNSS_MOSI_PIN; 
    spi_config.sck_pin  = SPI_SCK_PIN;

     
    nrf_gpio_cfg_output(GNSS_RST_PIN);          /* Set GNSS_RST pin as a output-pin,and set as high*/ 
    nrf_gpio_pin_set(GNSS_RST_PIN);

   
    nrf_gpio_cfg_output(GNSS_EXTINT_PIN);       /* Set GNSS_EXTINT_PIN as a output-pin,and set as high*/ 
    nrf_gpio_pin_set(GNSS_EXTINT_PIN);          


    nrf_gpio_cfg_output(GNSS_CS_PIN);           /* Set chip select as a output-pin,and set as high*/                                
    nrf_gpio_pin_set(GNSS_CS_PIN);              

    ret_code_t err_code = nrf_drv_spi_init(&spi, &spi_config,NULL, NULL);
    APP_ERROR_CHECK(err_code);

      GNSS_sleep();                              /*To make sure the GNSS function is in sleep when initialized */
  }


/* Puts the GPS into a sleep mode     
   By transfering bytes to the chip and sending it into low power consumption mode. Woken on EXTINT edge, SPI or UART.*/
void GNSS_sleep(void)                          
{
            
     /*Message UBX-RMX-PMREQ: Sending this message turns off the module indefinitely until one of four signals are received on the module.
        These are EXTINT0, EXTINT1 (external interrupts), or communication on the SPI or UART bus.< LEGG INN BESKRIVELSE*/
    uint8_t pmReq[24] = {0xB5, 0x62, 0x02, 0x41, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0xE8, 0x00, 0x00, 0x00, 0x3D, 0xCB};
    
    nrf_gpio_pin_clear(GNSS_CS_PIN);                                /* Enable the CS to transfer the message */
        
    nrf_drv_spi_transfer(&spi, pmReq, 24, receive_buf, 24);         /*Transfer message*/

    nrf_gpio_pin_set(GNSS_CS_PIN);                                  /* Disenable the CS to transfer the message */
          
    SEGGER_RTT_printf(0, "Good night GPS module. \r\n");            // For debugging purposes
}




void run_GNSS(void)
{
    nrf_gpio_pin_toggle(GNSS_EXTINT_PIN); //Wake the GNSS with an EXTINT signal 
    SEGGER_RTT_printf(0, "GPS module woken from sleep.\r \n"); 
    
    fix_flag = false;
    times_up = false;

    seconds_since_last_update = 0;
    seconds_since_start=app_timer_cnt_get()/8;

    /*Run the GPS until it gets fix, or until the timeout exits the loop*/
    while(!fix_flag){

        seconds_since_last_update =app_timer_cnt_get()/8;                             /* Get time before entering loop*/

        /*Update the GPS every 30 secs*/                                              
        while(((app_timer_cnt_get()/8)-seconds_since_last_update) < UPDATE_SEC ){

            GNSS_Read();                                                              /* Transfer data from the GNSS*/
            nrf_delay_ms(100);         
            GNSS_Sort_Out_Coordinates();                                              /* Sort out the coordinates*/
            nrf_delay_ms(100);
            
                                
            if(Check_If_Longitude_Is_Correct() && Check_If_Latitude_Is_Correct())     /* If the GPS got a position fix. Exit the inner while loop*/
            {    
                fix_flag = true;
                
                SEGGER_RTT_printf(0, "Got a fix! \r\n");
                lat = &latitude[0];                         
                lng = &longitude[0];
                break;
            }
        }
                
        if(!fix_flag){
            seconds_since_last_update = 0;
        }

        if((((app_timer_cnt_get()/8)-seconds_since_start) >= TIMES_UP) && !fix_flag){
            SEGGER_RTT_printf(0, "GPS timed out after %d seconds with no fix.\r\n", ((app_timer_cnt_get()/8)-seconds_since_start)); //Cold start uses up to 45 minutes to get a fix
            NRF_LOG_FLUSH();
            break;
        }
    }
    
    NRF_LOG_FLUSH();

    GNSS_sleep();
}


/* Pointer to the first element in lat*/
uint8_t * GNSS_get_lat_p(void)
{
  return &lat[0];                         
}

/* Pointer to the first element in lng*/
uint8_t * GNSS_get_lng_p(void)
{
  return &lng[0];                        
}

/* Used in man to check for fix*/
bool GNSS_has_fix(void)
{
  return fix_flag;
}
