#ifndef INIT_H
#define INIT_H

#include "utilities.h"
#include "beacon_buffer.h"
#include "beacon_scanner.h" 
#include "gateway_config.h"
#include "sdk_config.h"
#include "GNSS.h"



void log_init(void);
void power_management_init(void);



#endif