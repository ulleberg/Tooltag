# ToolTag
Systemet er en prototype som skal automatisere sporing og logistikkføring av verktøy for blant annet byggebransjen. Sporing av mobile basestasjoner (COW) for Telenor inngår som parallell use-case. Systemet er delt i ulike moduler. Systemet er basert på hardware fra Nordic Semiconductors og StalkIT, i tillegg til Bluetooth Low Energy for sporing av verktøy og NB-IoT fra Telenor for opplasting av data. Se fullstendig systembeskrivelse på wikien for ytterligere informasjon. Systemet ble utviklet av studenter fra Elektronisk sytemdesign og innovasjon på NTNU gjennom et sommerprosjekt i regi av Trådløse Trondheim. Hovedsamarbeidspartnere var Byggmesteran AS og Telenor ASA.

Ytterligere informasjon om prosjektet og demosider finnes på:

http://www.tooltag.no

http://telenor.tootlag.no

http://byggmesteran.tooltag.no


## Frontend
Brukergrensesnittet ble utarbeidet med Django 2.1. Se henholdsvis `mysite` for Byggmesteran-case og `telenor` for Telenor-case. Disse kan startes ved hjelp av 

```
python3 manage.py runserver
```

Se https://docs.djangoproject.com/en/2.1/intro/install/ for installasjon av Django. Merk at det er nødvendig å kjøre en MySQL-databaseserver for å teste disse.


## Backend

Serveren er basert på en Droplet fra DigitalOcean og kjører Debian 9.5 stretch. Nginx versjon 1.10.3 brukes som reverse-proxy med ulike virituelle serverblokker for håndtering av forespørsler via wildcard-DNS. uWSGI versjon 2.0.17.1 brukes som Gateway Interface for Python-baserte programmer. Nginx og uWSGI kommuniserer gjennom unix-sockets i `/run/uwsgi/`. Merk at eierskapet til disse skal være satt til `www-data`. uWSGI kjører som en daemon gjennom `systemd` og er konfigurert i en såkalt emperor-mode, der den genererer og styrer workers for å håndtere innkommende forespørsler. uWSGI krever at en .ini-fil gjøres tilgjengelig for hver webapplikasjon, i tillegg til en emperor.ini `/etc/uwsgi/` som lister opp disse.

En parser brukes for å hente data i JSON-format fra Telenors backend for NB-IoT, HORDE. Denne kjøres automatisk hver time gjennom en cronjob. Merk at oppdateringsfrekvensen er satt til å være så lav, da Telenors backend på nåverende stadie krever at alle data lastes ned, uten mulighet for å slettes. Dette er forventet å forbedres i løpet av oktober, med eget REST-API.

Parseren legger dataene i en MariaDB-drevet database. Modeller kan åpnes ved hjelp av MySQL Workbench. SQL CREATE-skript er også tilgjengelige, med og uten data for testing.


## Gateway

Gateway håndterer detektering av beacons, posisjonering via GPS og opplasting av data gjennom NB-IoT ved hjelp av en SARA-modul. Gatewayen som ble realisert gjennom `stalkIT Tracker Prototype v2`. Den har;
- En MCU, nrf52832
- CAM-M8Q (GNSS)
- SARA-N210 (RADIO)
- PSU
- Noen sensorer

I tillegg til hardware brukes litt forskjellig software:
- Nordic Semiconductor sitt SDK v15.0.0, eksempel peripheral/spi
- IDE: SEGGER Embedded Studio for ARM V3.40
- Programmering av StalkIT trenger en ekstern 10-pin debug-tilkobler
- Vi koblet debug-tilkoblingen til Nordic sitt PCA10040 DK og flashet til minnet via den
- J-Link RTT viewer brukes til debugging. RTT-meldinger printes i koden, og kan leses i RTT Viewer


## Beacon

Beacons ble prototypet på Nordics Smart Beacon Kit. Spesifikasjoner for senderene;
- PCA20006
- SDK v10.0.0
- Eddystone eksempel på Keil 5 MDK
- Softdevice s110
- nRF41522 CEAAE00, (256 kB flash, 16 kB RAM)
- IROM1, Start: 0x18000  Size: 0x28000
- IRAM1, Start: 0x20002000  Size: 0x2000
