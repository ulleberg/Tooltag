import requests, json, base64, pymysql, csv
from math import sin, cos, sqrt, atan2, radians, acos, sqrt

# Set path to correct working directory for file operations
import os.path
path = os.path.dirname(__file__)
os.chdir(path)

# Database connection settings
db = pymysql.connect("tooltag.no", "demouser", "demopw", "demo")
cursor = db.cursor()

# URL to fetch JSON data from
target_url = 'https://in.nbiot.engineering/devices/{}/data'

# Credentials for HTTPAuth
username = 'test'
password = 'nbiot'

# Process only data from whitelisted IMEI after a certain timestamp
imei_whitelist = [357517080119474]
last_timestamp = 1533642944187664228

# Fetch the data from the web server
def fetch_json(imei):
	url = target_url.format(str(imei)) # Fetch JSON for given IMEI
	response = requests.get(url, auth=(username, password))
	return response.json()

# Extracts and decodes payloads with correct timestamp
def extract_data(data, last_timestamp):
	payloads = []
	for line in data['data']:
		if int(line['timestamp']) > int(last_timestamp):
			last_timestamp = line['timestamp']
			payload = base64.b64decode(line['payload']).decode('utf-8')
			payloads.append(payload.split(';')[:-1]) # Paylaod string ends in semicolon. Remove last element
	return (payloads, last_timestamp) # Return the payloads and last timestamp

# Iterates through list of projects and finds closest distance to project
def find_closest_project(long, lat, projects):
	closest_project_id = 0
	smallest_distance = 100000000000 # acceptable deviation
	for project in projects:
		radius = get_distance(float(long), float(lat), float(project[1]), float(project[2]))
		# print('radius', radius, 'project', project[0])
		if radius < smallest_distance:
			smallest_distance = radius
			closest_project_id = project[0]
	return (closest_project_id, smallest_distance)

# Gets radius based on coordinates
def get_radius(x1, y1, x2, y2):
	meters_per_degree = 111226
	return sqrt( ( x1 - x2 )**2 + ( y1 - y2  )**2 ) * meters_per_degree

def get_distance(lon1, lat1, lon2, lat2):
	R = 6353000.0
	lat1 = radians(lat1)
	lon1 = radians(lon1)
	lat2 = radians(lat2)
	lon2 = radians(lon2)

	#converts to Cartesian coordinates
	x1 ,y1, z1 = cos(lat1)*cos(lon1), cos(lat1)*sin(lon1), sin(lat1)
	x2 ,y2, z2 = cos(lat2)*cos(lon2), cos(lat2)*sin(lon2),  sin(lat2)

	#calculeates the difference in angel between the coordinates using the formula
	# u*v = |u|*|v|*cos(w)
	d_angel = acos(x1*x2 +y1*y2 + z1*z2)

	#returning the arc length
	return R * d_angel

# Updates the measurements table in the database
def update_measurements(tool_id, project_id, gateway_id):
	sql = """
	SELECT a.tool_id, a.project_id, a.gateway_id, a.last_seen
	FROM measurements AS a
	INNER JOIN (
		SELECT tool_id, MAX(last_seen) as ls
		FROM measurements
		GROUP BY tool_id
	) AS b WHERE a.tool_id = b.tool_id AND a.last_seen = b.ls
	AND a.tool_id = {} AND a.project_id = {}
	""".format(tool_id, project_id)
	# Fetch latest entry for tool_id
	cursor.execute(sql)
	data = cursor.fetchone()
	if data == None: # no matching record found, make new
		sql = """
		INSERT INTO measurements (measurements.tool_id, measurements.project_id, measurements.gateway_id, measurements.last_seen)
		VALUES ({}, {}, {}, NOW());
		""".format(tool_id, project_id, gateway_id)
		try:
			cursor.execute(sql)
			db.commit()
		except:
			print('SQL ERROR AT MEASURE CREATION')
			db.rollback()
	else: # update timestamp and gateway
		last_seen = data[3]
		sql = """
		UPDATE measurements
		SET measurements.gateway_id = {}, measurements.last_seen = NOW()
		WHERE measurements.tool_id = {} AND measurements.last_seen = '{}';
		""".format(gateway_id, tool_id, last_seen)
		try:
			cursor.execute(sql)
			db.commit()
		except:
			print('SQL ERROR AT MEASURE UPDATE')
			db.rollback()

# Finds correct tool_id by UID
def get_tool_id(uid):
	sql = "SELECT tools.tool_id FROM tools WHERE tools.uid = {}".format(uid)
	try:
		cursor.execute(sql)
		db.commit()
		data = cursor.fetchone()
		return int(data[0])
	except:
		db.rollback()

# Find currect gateway_id by UID
def get_gateway_id(uid):
	sql = "SELECT gateways.gateway_id FROM gateways WHERE gateways.gateway_uid = {}".format(uid)
	try:
		cursor.execute(sql)
		db.commit()
		data = cursor.fetchone()
		return int(data[0])
	except:
		db.rollback()

# Update gateway location
def update_gateway(gateway_id, long, lat):
	sql = """
	UPDATE gateways
	SET gateways.long = {}, gateways.lat = {}
	WHERE gateways.gateway_uid = {}
	""".format(long, lat, gateway_id)
	try:
		cursor.execute(sql)
		db.commit()
	except:
		print('SQL ERROR AT GATEWAY UPDATE')
		db.rollback()

# Fetches list of all projects and their positions
def get_project_positions():
	sql = """
	SELECT projects.project_id, addresses.long, addresses.lat
	FROM projects
	INNER JOIN addresses on addresses.address_id = projects.project_id;
	"""

	cursor.execute(sql)
	data = cursor.fetchall()
	return data

# Split tool_id and RSSI
def split_beacon_id(beacon_id):
	tool_id = int(beacon_id[0:-2], 16)
	rssi = -int(beacon_id[-2:], 16) # Convert back to negative number [dB]
	return (tool_id, rssi)

# Converts coordinates to decimal degrees
def coordinate_converter(coordinate_str):
	lst = coordinate_str.split('.')
	degrees = int(lst[0][:-2])
	minutes = int(lst[0][-2:]) / 60
	minutes_decimals = float('0.' + lst[1]) / 60
	decimal_degrees = degrees + minutes + minutes_decimals
	return decimal_degrees

# Fetch IMEI and timestamps from file
def get_whitelist():
	whitelist = []
	with open('whitelist.csv') as f:
		lines = f.readlines()
		for line in lines:
			line = line.strip().split(',')
			whitelist.append( (line[0], line[1]) )
	return whitelist

# Update the whitelist file
def write_whitelist(whitelist):
	with open('whitelist.csv', 'w') as f:
		for line in whitelist:
			output = str(line[0]) + ',' + str(line[1]) + '\n'
			f.write(output)

# Main control flow
if __name__ == "__main__":
	new_whitelist = []
	for (imei,timestamp) in get_whitelist():
		data = fetch_json(imei)
		(payloads, last_timestamp) = extract_data(data, timestamp) # Extract and decode newest payloads
		new_whitelist.append((imei, last_timestamp))
		# Iterate through payloads and handle data
		projects = get_project_positions()
		for payload in payloads:
			gateway_uid = int(payload[0], 16) # Convert from hex
			gateway_id = get_gateway_id(gateway_uid)
			long = coordinate_converter(payload[1])
			lat = coordinate_converter(payload[2])
			(project_id, distance) = find_closest_project(long, lat, projects)
			beacons = payload[3:] # Get list of beacons from payload
			# TODO: Implement check if inside project radius
			for beacon_id in beacons:
				(uid, rssi) = split_beacon_id(beacon_id)
				tool_id = get_tool_id(uid)
				if tool_id != None: # Check if ID was found.
					print('tool_id:', tool_id,'project_id:',project_id, 'tool_uid', uid,'gateway_id:', gateway_id, 'gateway_uid', gateway_uid)
					update_measurements(tool_id, project_id, gateway_id)
					update_gateway(gateway_id, long, lat)
				else:
					print('WARNING. TOOL_ID NOT FOUND', uid)
	write_whitelist(new_whitelist)
