-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 10. Aug, 2018 15:09 PM
-- Server-versjon: 10.1.26-MariaDB-0+deb9u1
-- PHP Version: 7.0.30-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `addresses`
--

CREATE TABLE `addresses` (
  `address_id` int(10) UNSIGNED NOT NULL,
  `street` varchar(45) DEFAULT NULL,
  `postal_code` int(11) DEFAULT NULL,
  `long` decimal(10,7) DEFAULT NULL,
  `lat` decimal(10,7) DEFAULT NULL,
  `place_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `addresses`
--

INSERT INTO `addresses` (`address_id`, `street`, `postal_code`, `long`, `lat`, `place_name`) VALUES
(1, 'Russervegen 4', 7650, '11.4554300', '63.7860510', 'Byggmesteran AS'),
(2, 'Elgsetergate 1', 7030, '10.3949543', '63.4225954', '<not implemented>'),
(3, 'Hammerøyvegen 4', 7630, '11.0496455', '63.6128488', 'Åsen'),
(4, 'Sundssandvegen 13', 7670, '11.2859184', '63.8654914', '<not implemented>'),
(5, 'Friggs veg 2', 7602, '11.2724779', '63.7549031', 'Gjemble'),
(6, 'Sluppenveien 5', 7073, '101.7423841', '34.0858642', '<not implemented>');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`) VALUES
(1, 'strøm og skap'),
(2, 'sager'),
(3, 'vinkelslipere'),
(4, 'høvler'),
(5, 'vifteovner'),
(6, 'pistoler'),
(7, 'kompressorer'),
(8, 'muttertrekkere'),
(9, 'avfuktere'),
(10, 'skrumaskiner strøm'),
(11, 'støvsugere'),
(12, 'skrumaskiner stål'),
(13, 'meiselmaskiner'),
(14, 'båndslipere'),
(15, 'laminat- og parkettkuttere'),
(16, 'container'),
(17, 'diverse');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `contacts`
--

CREATE TABLE `contacts` (
  `contact_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `phone` int(11) NOT NULL,
  `email` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `contacts`
--

INSERT INTO `contacts` (`contact_id`, `name`, `phone`, `email`) VALUES
(1, 'Andor Jermstad', 92895368, 'andor@byggmesteran.no'),
(2, 'Morten Øyen', 91773410, 'morten@byggmesteran.no'),
(3, 'Espen Matberg', 40073370, 'epsen@byggmesteran.no'),
(4, 'Joar Rusten', 98896434, 'joar@tradlosetrondheim.no'),
(5, 'Thomas Ulleberg', 92616218, 'thomas@tradlosetrondheim.no'),
(6, 'Peter Uran', 48601193, 'pmuran@stud.ntnu.no');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `gateways`
--

CREATE TABLE `gateways` (
  `gateway_id` int(10) UNSIGNED NOT NULL,
  `gateway_uid` int(11) DEFAULT NULL,
  `gateway_name` varchar(45) DEFAULT NULL,
  `long` decimal(10,7) DEFAULT NULL,
  `lat` decimal(10,7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `gateways`
--

INSERT INTO `gateways` (`gateway_id`, `gateway_uid`, `gateway_name`, `long`, `lat`) VALUES
(1, 43981, 'gw1', '11.4532230', '63.7860430'),
(2, 43962, 'gw2', '10.3978470', '63.3968346'),
(3, NULL, 'gw3', '11.0496450', '63.6128488'),
(4, NULL, 'gw4', '11.1496450', '63.6228488'),
(5, NULL, 'gw5', '11.2724779', '63.7549031');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `measurements`
--

CREATE TABLE `measurements` (
  `tool_id` int(10) UNSIGNED NOT NULL,
  `gateway_id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL,
  `last_seen` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `measurements`
--

INSERT INTO `measurements` (`tool_id`, `gateway_id`, `project_id`, `last_seen`) VALUES
(1, 2, 5, '2018-07-13 13:01:14'),
(2, 3, 3, '2018-07-13 13:01:14'),
(3, 1, 4, '2018-07-13 13:01:14'),
(4, 1, 3, '2018-07-13 13:01:14'),
(5, 1, 4, '2018-07-13 13:01:14'),
(6, 4, 3, '2018-07-13 13:01:14'),
(7, 5, 2, '2018-07-13 13:01:14'),
(8, 3, 3, '2018-07-13 13:01:14'),
(9, 2, 1, '2018-07-13 13:01:15'),
(10, 4, 1, '2018-07-13 13:01:15'),
(11, 1, 3, '2018-07-13 13:01:15'),
(12, 2, 5, '2018-07-13 13:01:15'),
(13, 5, 1, '2018-07-13 13:01:15'),
(14, 1, 4, '2018-07-13 13:01:15'),
(15, 2, 4, '2018-07-13 13:01:15'),
(16, 3, 5, '2018-07-13 13:01:15'),
(17, 5, 3, '2018-07-13 13:01:15'),
(18, 1, 3, '2018-07-13 13:01:15'),
(19, 3, 2, '2018-07-13 13:01:15'),
(20, 4, 3, '2018-07-13 13:01:15'),
(21, 2, 5, '2018-07-13 13:01:15'),
(22, 2, 4, '2018-07-13 13:01:15'),
(23, 2, 2, '2018-07-13 13:01:15'),
(24, 1, 4, '2018-07-13 13:01:15'),
(25, 5, 1, '2018-07-13 13:01:15'),
(26, 1, 3, '2018-07-13 13:01:15'),
(27, 3, 4, '2018-07-13 13:01:15'),
(28, 2, 5, '2018-07-13 13:01:15'),
(29, 4, 5, '2018-07-13 13:01:15'),
(30, 2, 4, '2018-07-13 13:01:15'),
(31, 5, 3, '2018-07-13 13:01:15'),
(32, 4, 1, '2018-07-13 13:01:15'),
(33, 4, 5, '2018-07-13 13:01:15'),
(34, 3, 2, '2018-07-13 13:01:15'),
(35, 3, 4, '2018-07-13 13:01:15'),
(36, 3, 4, '2018-07-13 13:01:15'),
(37, 5, 5, '2018-07-13 13:01:15'),
(38, 2, 2, '2018-07-13 13:01:15'),
(1, 1, 2, '2018-07-13 13:12:31'),
(2, 3, 2, '2018-07-13 13:12:31'),
(3, 2, 3, '2018-07-13 13:12:31'),
(4, 2, 1, '2018-07-13 13:12:31'),
(5, 5, 1, '2018-07-13 13:12:31'),
(6, 2, 5, '2018-07-13 13:12:31'),
(7, 2, 3, '2018-07-13 13:12:31'),
(8, 2, 2, '2018-07-13 13:12:31'),
(9, 4, 4, '2018-07-13 13:12:31'),
(10, 3, 4, '2018-07-13 13:12:31'),
(11, 3, 3, '2018-07-13 13:12:31'),
(12, 1, 2, '2018-07-13 13:12:31'),
(13, 3, 4, '2018-07-13 13:12:31'),
(14, 3, 3, '2018-07-13 13:12:31'),
(15, 1, 3, '2018-07-13 13:12:31'),
(16, 4, 2, '2018-07-13 13:12:31'),
(17, 5, 4, '2018-07-13 13:12:31'),
(18, 2, 5, '2018-07-13 13:12:31'),
(19, 4, 5, '2018-07-13 13:12:31'),
(20, 4, 3, '2018-07-13 13:12:31'),
(21, 5, 1, '2018-07-13 13:12:31'),
(22, 3, 5, '2018-07-13 13:12:31'),
(23, 1, 2, '2018-07-13 13:12:31'),
(24, 2, 3, '2018-07-13 13:12:31'),
(25, 3, 2, '2018-07-13 13:12:31'),
(26, 1, 4, '2018-07-13 13:12:31'),
(27, 4, 4, '2018-07-13 13:12:31'),
(28, 2, 3, '2018-07-13 13:12:31'),
(29, 2, 3, '2018-07-13 13:12:31'),
(30, 3, 5, '2018-07-13 13:12:31'),
(31, 3, 4, '2018-07-13 13:12:31'),
(32, 4, 3, '2018-07-13 13:12:31'),
(33, 1, 1, '2018-07-13 13:12:31'),
(34, 3, 4, '2018-07-13 13:12:31'),
(35, 1, 1, '2018-07-13 13:12:31'),
(36, 1, 2, '2018-07-13 13:12:31'),
(37, 4, 5, '2018-07-13 13:12:31'),
(38, 5, 3, '2018-07-13 13:12:31'),
(1, 1, 5, '2018-07-13 13:41:38'),
(2, 2, 3, '2018-07-13 13:41:38'),
(3, 5, 3, '2018-07-13 13:41:38'),
(4, 5, 4, '2018-07-13 13:41:38'),
(5, 5, 2, '2018-07-13 13:41:38'),
(6, 2, 3, '2018-07-13 13:41:38'),
(7, 5, 3, '2018-07-13 13:41:38'),
(8, 2, 5, '2018-07-13 13:41:38'),
(9, 5, 2, '2018-07-13 13:41:38'),
(10, 4, 1, '2018-07-13 13:41:38'),
(11, 2, 5, '2018-07-13 13:41:38'),
(12, 5, 1, '2018-07-13 13:41:38'),
(13, 5, 4, '2018-07-13 13:41:38'),
(14, 2, 1, '2018-07-13 13:41:38'),
(15, 1, 5, '2018-07-13 13:41:38'),
(16, 1, 4, '2018-07-13 13:41:38'),
(17, 5, 3, '2018-07-13 13:41:38'),
(18, 3, 5, '2018-07-13 13:41:38'),
(19, 4, 2, '2018-07-13 13:41:38'),
(20, 4, 2, '2018-07-13 13:41:38'),
(21, 1, 5, '2018-07-13 13:41:38'),
(22, 4, 1, '2018-07-13 13:41:38'),
(23, 5, 5, '2018-07-13 13:41:38'),
(24, 5, 3, '2018-07-13 13:41:38'),
(25, 5, 4, '2018-07-13 13:41:38'),
(26, 5, 3, '2018-07-13 13:41:38'),
(27, 1, 4, '2018-07-13 13:41:38'),
(28, 5, 3, '2018-07-13 13:41:38'),
(29, 4, 4, '2018-07-13 13:41:38'),
(30, 5, 1, '2018-07-13 13:41:38'),
(31, 1, 2, '2018-07-13 13:41:38'),
(32, 5, 3, '2018-07-13 13:41:38'),
(33, 5, 5, '2018-07-13 13:41:38'),
(34, 2, 4, '2018-07-13 13:41:38'),
(35, 2, 1, '2018-07-13 13:41:38'),
(36, 2, 2, '2018-07-13 13:41:38'),
(37, 5, 3, '2018-07-13 13:41:38'),
(38, 2, 4, '2018-07-13 13:41:38'),
(1, 3, 3, '2018-07-15 06:17:31'),
(2, 2, 2, '2018-07-15 06:17:31'),
(3, 4, 4, '2018-07-15 06:17:31'),
(4, 1, 1, '2018-07-15 06:17:31'),
(5, 5, 1, '2018-07-15 06:17:31'),
(6, 1, 5, '2018-07-15 06:17:31'),
(7, 2, 4, '2018-07-15 06:17:31'),
(8, 5, 1, '2018-07-15 06:17:31'),
(9, 3, 3, '2018-07-15 06:17:31'),
(10, 2, 3, '2018-07-15 06:17:31'),
(11, 5, 2, '2018-07-15 06:17:31'),
(12, 2, 5, '2018-07-15 06:17:31'),
(13, 1, 3, '2018-07-15 06:17:31'),
(14, 3, 1, '2018-07-15 06:17:31'),
(15, 5, 1, '2018-07-15 06:17:31'),
(16, 1, 4, '2018-07-15 06:17:31'),
(17, 2, 3, '2018-07-15 06:17:31'),
(18, 5, 4, '2018-07-15 06:17:31'),
(19, 1, 3, '2018-07-15 06:17:31'),
(20, 5, 3, '2018-07-15 06:17:31'),
(21, 2, 5, '2018-07-15 06:17:31'),
(22, 4, 1, '2018-07-15 06:17:31'),
(23, 3, 4, '2018-07-15 06:17:31'),
(24, 1, 2, '2018-07-15 06:17:31'),
(25, 3, 1, '2018-07-15 06:17:31'),
(26, 3, 5, '2018-07-15 06:17:31'),
(27, 1, 4, '2018-07-15 06:17:31'),
(28, 3, 5, '2018-07-15 06:17:31'),
(29, 4, 3, '2018-07-15 06:17:31'),
(30, 4, 1, '2018-07-15 06:17:31'),
(31, 1, 2, '2018-07-15 06:17:31'),
(32, 5, 5, '2018-07-15 06:17:31'),
(33, 1, 4, '2018-07-15 06:17:31'),
(34, 1, 1, '2018-07-15 06:17:31'),
(35, 3, 2, '2018-07-15 06:17:31'),
(36, 2, 4, '2018-07-15 06:17:31'),
(37, 1, 4, '2018-07-15 06:17:31'),
(38, 5, 2, '2018-07-15 06:17:31'),
(24, 1, 6, '2018-08-08 17:59:21'),
(21, 1, 6, '2018-08-08 17:59:21'),
(12, 1, 6, '2018-08-08 17:59:20'),
(15, 1, 6, '2018-08-08 17:59:21'),
(24, 2, 1, '2018-08-08 18:01:04'),
(21, 2, 1, '2018-08-08 18:01:04'),
(12, 2, 1, '2018-08-08 18:01:04'),
(15, 2, 1, '2018-08-08 18:01:04'),
(24, 1, 6, '2018-08-08 18:03:59'),
(21, 1, 6, '2018-08-08 18:03:59'),
(12, 1, 6, '2018-08-08 18:03:59'),
(15, 1, 6, '2018-08-08 18:03:59'),
(24, 2, 1, '2018-08-09 07:10:37'),
(21, 2, 1, '2018-08-09 07:10:37'),
(12, 2, 1, '2018-08-09 07:10:37'),
(15, 2, 1, '2018-08-09 07:10:37'),
(24, 1, 6, '2018-08-09 07:14:51'),
(21, 1, 6, '2018-08-09 07:14:51'),
(12, 1, 6, '2018-08-09 07:14:51'),
(15, 1, 6, '2018-08-09 07:14:51'),
(30, 1, 6, '2018-08-09 07:33:52'),
(30, 2, 1, '2018-08-09 07:38:22'),
(24, 2, 1, '2018-08-09 07:38:22'),
(21, 2, 1, '2018-08-09 07:38:22'),
(12, 2, 1, '2018-08-09 07:38:22'),
(15, 2, 1, '2018-08-09 07:39:00'),
(21, 1, 6, '2018-08-09 07:43:44'),
(12, 1, 6, '2018-08-09 07:43:44'),
(15, 1, 6, '2018-08-09 07:43:44'),
(30, 1, 6, '2018-08-09 07:43:44'),
(24, 1, 6, '2018-08-09 07:43:44'),
(12, 2, 1, '2018-08-10 14:14:09'),
(15, 2, 1, '2018-08-10 14:14:09'),
(30, 2, 1, '2018-08-10 14:14:09'),
(24, 2, 1, '2018-08-10 14:14:09'),
(21, 2, 1, '2018-08-10 14:14:09');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `projects`
--

CREATE TABLE `projects` (
  `project_id` int(10) UNSIGNED NOT NULL,
  `contact_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `finish_date` datetime DEFAULT NULL,
  `note` varchar(400) DEFAULT NULL,
  `address_id` int(10) UNSIGNED NOT NULL,
  `is_finished` tinyint(1) NOT NULL,
  `radius` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `projects`
--

INSERT INTO `projects` (`project_id`, `contact_id`, `name`, `start_date`, `finish_date`, `note`, `address_id`, `is_finished`, `radius`) VALUES
(1, 3, 'Lager', '2018-07-13 00:00:00', '2018-07-13 00:00:00', 'Gjør nye innkjøp på spikerpistoler.', 1, 0, 100),
(2, 1, 'Åsen barnehage', '2018-03-04 00:00:00', '2018-10-31 00:00:00', 'Husk leketøy.', 3, 1, 400),
(3, 2, 'Takbytte Samfundet', '2018-05-16 00:00:00', '2018-11-15 00:00:00', 'Trenger flere planker.', 2, 1, 200),
(4, 3, 'Straumen Park', '2018-09-10 00:00:00', '2019-02-14 00:00:00', 'Fordelingsskap ødelagt. Send nytt.', 4, 0, 200),
(5, 1, 'Tomannbolig Gjemble', '2018-09-19 00:00:00', '2019-04-25 00:00:00', 'Har bolig, mangler to menn,', 5, 1, 200),
(6, 5, 'Fjern tagging', '2018-07-16 00:00:00', '2018-08-18 00:00:00', 'Veldig viktigTooltag', 6, 0, 100);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `tools`
--

CREATE TABLE `tools` (
  `tool_id` int(10) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED DEFAULT NULL,
  `tool_name` varchar(45) DEFAULT NULL,
  `manufacturer` varchar(45) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  `service_date` datetime DEFAULT NULL,
  `purchased_date` datetime DEFAULT NULL,
  `rssi` int(11) DEFAULT NULL,
  `uid` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `tools`
--

INSERT INTO `tools` (`tool_id`, `type_id`, `tool_name`, `manufacturer`, `note`, `service_date`, `purchased_date`, `rssi`, `uid`) VALUES
(1, 1, '4-1', 'hitachi', 'ok', '2019-03-12 13:00:00', '2017-09-10 00:00:00', NULL, NULL),
(2, 1, '4-2', 'tjep', 'dårlig batteri', '2019-01-14 10:00:00', '2018-05-13 09:00:00', NULL, NULL),
(3, 1, '4-3', 'hitachi', 'ok', '2018-07-25 13:00:00', '2017-04-11 10:00:00', NULL, NULL),
(4, 1, '4-7', 'basso', 'ok', '2018-07-26 14:00:00', '2018-02-11 09:00:00', NULL, NULL),
(5, 18, '3-11', 'basso', NULL, '2018-07-26 13:00:00', '2014-10-12 12:00:00', NULL, 246706685927520),
(6, 20, '6-1', 'hitachi', 'ok', '2018-07-26 14:00:00', '2018-01-14 16:00:00', NULL, NULL),
(7, 22, '7-1', 'hitachi', 'i ustand', '2018-07-26 00:00:00', '2018-03-15 00:00:00', NULL, NULL),
(8, 5, '29-9', 'basso', '', '2018-07-26 14:00:00', '2018-03-15 15:00:00', NULL, NULL),
(9, 49, '37-7', 'cdt etman', 'ok', '2018-07-26 15:00:00', '2018-03-15 13:00:00', NULL, NULL),
(10, 56, 'NR 8', NULL, 'stor', NULL, '2018-03-15 13:00:00', NULL, NULL),
(11, 1, '4-4', 'hitachi', 'ok', '2019-02-13 00:00:00', '2016-02-13 00:00:00', NULL, NULL),
(12, 1, '4-5', 'hitachi', 'ok', '2019-02-13 00:00:00', '2016-02-13 00:00:00', NULL, 15397692706305),
(13, 1, '4-6', 'hitachi', 'ok', '2019-02-13 00:00:00', '2017-04-13 00:00:00', NULL, NULL),
(14, 1, '4-8', 'Basso', 'i ustand', '2019-02-13 00:00:00', '2016-02-13 00:00:00', NULL, NULL),
(15, 1, '4-9', 'Basso', 'ok. ', '2020-02-13 00:00:00', '2017-02-13 00:00:00', NULL, 15406282771971),
(16, 1, '4-10', 'Basso', 'ok. ', '2019-02-13 00:00:00', '2009-09-13 00:00:00', NULL, NULL),
(17, 1, '4-11', 'Basso', 'Mangler batteri. ', '2019-02-13 00:00:00', '2008-04-13 00:00:00', NULL, NULL),
(18, 1, '16-1', 'DX 460 ', 'ok', '2019-05-13 00:00:00', '2014-05-13 00:00:00', NULL, NULL),
(19, 3, '16-2', 'DX 460', 'ok ', '2019-05-13 00:00:00', '2014-02-13 00:00:00', NULL, NULL),
(20, 3, '16-3', 'DX 460', 'Må inn på lager. ', '2020-01-13 00:00:00', '2015-05-13 00:00:00', NULL, NULL),
(21, 3, '16-9', 'BX3', 'ok', '2020-02-13 00:00:00', '2017-05-13 00:00:00', NULL, 15401987739138),
(22, 5, '29-1', 'Basso', 'ok', '2020-02-13 00:00:00', '2018-02-13 00:00:00', NULL, NULL),
(23, 5, '29-2', 'Basso', 'S500/40-F1', '2020-02-13 00:00:00', '2017-09-25 00:00:00', NULL, NULL),
(24, 3, '34-1', 'HILTI DX 76 PTR', 'Trengs på Aasen.', '2020-06-04 00:00:00', '2018-05-09 00:00:00', NULL, 15410577804804),
(25, 7, '67-1', 'Dewalt', 'Husk batteri. ', '2020-02-13 00:00:00', '2016-02-13 00:00:00', NULL, NULL),
(26, 7, '67-2', 'Dewalt', 'Husk ekstra batteri', '2020-02-13 00:00:00', '2016-03-15 00:00:00', NULL, NULL),
(27, 7, '67-3', 'Dewalt', 'Husk luft', '2020-07-10 00:00:00', '2016-02-10 00:00:00', NULL, NULL),
(28, 7, '67-7', 'Dewalt', 'Husk luft. ', '2020-04-13 00:00:00', '2010-01-01 00:00:00', NULL, NULL),
(29, 1, '67-10', 'Dewalt', 'James Bond var her', '2026-02-13 00:00:00', '2013-10-13 00:00:00', NULL, NULL),
(30, 56, 'NR 1', 'Containern', 'Liten', '2016-02-13 00:00:00', '2016-02-13 00:00:00', NULL, 15414872837637),
(31, 56, 'NR 2', 'Containern', 'ok', '2016-02-13 00:00:00', '2016-02-13 00:00:00', NULL, NULL),
(32, 21, '9-1', 'Metabo', 'ok', '2020-02-13 00:00:00', '2016-02-13 00:00:00', NULL, NULL),
(33, 21, '9-2', 'Dewalt', 'ok', '2020-02-13 00:00:00', '2016-02-13 00:00:00', NULL, NULL),
(34, 21, '9-10', 'Hitachi', 'ok', '2020-02-13 00:00:00', '2016-02-13 00:00:00', NULL, NULL),
(35, 21, '9-11', 'Dewalt', 'ok', '2020-02-13 00:00:00', '2016-02-13 00:00:00', NULL, NULL),
(36, 21, '9-12', 'ELU', 'mangler batteri', '2020-02-13 00:00:00', '2016-02-13 00:00:00', NULL, NULL),
(37, 45, '11-2', 'ukjent', 'Høvler så det suser.', '2020-02-14 00:00:00', '2013-02-13 00:00:00', NULL, NULL),
(38, 45, '11-3', 'ukjent', NULL, NULL, NULL, NULL, NULL),
(40, 6, NULL, 'mulle mekk as', 'kjøpt på loppemarked', '2020-08-22 00:00:00', '2017-12-24 00:00:00', NULL, NULL),
(41, 1, 'peter', 'uran', 'test', '2020-10-10 00:00:00', '2018-08-07 00:00:00', NULL, 1234),
(42, 1, 'p2', 'u2', 'test2', '0000-00-00 00:00:00', '2018-08-07 00:00:00', NULL, 3234),
(43, 1, 'p3', 'u3', 'test3', '0000-00-00 00:00:00', '2018-08-07 00:00:00', NULL, 3234),
(44, 2, 'p5', 'u4', 'testu', '0000-00-00 00:00:00', '2018-08-07 00:00:00', NULL, 3234);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `types`
--

CREATE TABLE `types` (
  `type_id` int(10) UNSIGNED NOT NULL,
  `type_name` varchar(45) DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `types`
--

INSERT INTO `types` (`type_id`, `type_name`, `category_id`) VALUES
(1, 'spikerpistoler', 6),
(2, 'spikerpistoler papp', 6),
(3, 'boltepistoler ', 6),
(4, 'blåsepistoler', 6),
(5, 'krampepistoler', 6),
(6, 'varmluftpistoler', 6),
(7, 'fugepistoler', 6),
(8, 'dykkertpistoler /m gass', 6),
(9, 'dykkertpistoler /m batteri', 6),
(10, 'dykkertpistoler EL', 6),
(11, 'dykkertspistoler mini', 6),
(12, 'fordeler', 1),
(13, 'fordelingsskap', 1),
(14, '400 V gris', 1),
(15, 'trafo', 1),
(16, '5\"', 3),
(17, '9\"', 2),
(18, 'kjedesager', 2),
(19, 'sirkelsager', 3),
(20, 'håndsirkelsager', 2),
(21, 'stikksager', 2),
(22, 'gjerdesager', 2),
(23, 'hullsager', 2),
(24, 'hullsager 20 mm', 2),
(25, 'kapp- og gjærsager', 2),
(26, 'feinsager', 2),
(27, 'dykksager', 2),
(28, 'bajonettsager', 2),
(29, 'klyvsager', 2),
(30, 'kikkerter', 17),
(31, 'slagbormaskiner', 17),
(32, 'jekktraller', 17),
(33, 'traller', 17),
(34, 'søppelvogner', 17),
(35, 'stikkspader', 17),
(36, 'plasmabrennere', 17),
(37, 'flatskuffere', 17),
(38, 'skjærebord', 17),
(39, 'vogner m/ fire hjul', 17),
(40, 'jerrykanner ', 17),
(41, 'svabler', 17),
(42, 'limtvingere', 17),
(43, 'metallstang stor', 17),
(44, 'nibblere', 17),
(45, 'høvler', 4),
(46, 'vifteovner', 5),
(47, 'kompressorer', 7),
(48, 'muttertrekkere', 8),
(49, 'avfuktere', 9),
(50, 'skrumaskiner strøm', 10),
(51, 'støvsugere', 11),
(52, 'skrumaskiner stål', 12),
(53, 'meiselmaskiner ', 13),
(54, 'båndslipere', 14),
(55, 'laminat- og parkettkuttere ', 15),
(56, 'containere', 16),
(57, 'sikringsskap', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`address_id`),
  ADD UNIQUE KEY `adress_id_UNIQUE` (`address_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `gateways`
--
ALTER TABLE `gateways`
  ADD PRIMARY KEY (`gateway_id`),
  ADD UNIQUE KEY `gateway_id_UNIQUE` (`gateway_id`);

--
-- Indexes for table `measurements`
--
ALTER TABLE `measurements`
  ADD KEY `fk_measurements_tools1_idx` (`tool_id`),
  ADD KEY `fk_measurements_gateways1_idx` (`gateway_id`),
  ADD KEY `fk_measurements_projects1_idx` (`project_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`),
  ADD UNIQUE KEY `project_id_UNIQUE` (`project_id`),
  ADD KEY `fk_projects_contacts1_idx` (`contact_id`),
  ADD KEY `fk_projects_adresses1_idx` (`address_id`);

--
-- Indexes for table `tools`
--
ALTER TABLE `tools`
  ADD PRIMARY KEY (`tool_id`),
  ADD UNIQUE KEY `tool_id_UNIQUE` (`tool_id`),
  ADD KEY `fk_tools_types1_idx` (`type_id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`type_id`),
  ADD KEY `fk_types_categories1_idx` (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `address_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `contact_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `gateways`
--
ALTER TABLE `gateways`
  MODIFY `gateway_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tools`
--
ALTER TABLE `tools`
  MODIFY `tool_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `measurements`
--
ALTER TABLE `measurements`
  ADD CONSTRAINT `fk_measurements_gateways1` FOREIGN KEY (`gateway_id`) REFERENCES `gateways` (`gateway_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_measurements_projects1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_measurements_tools1` FOREIGN KEY (`tool_id`) REFERENCES `tools` (`tool_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Begrensninger for tabell `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `fk_projects_adresses1` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`address_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_projects_contacts1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`contact_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Begrensninger for tabell `tools`
--
ALTER TABLE `tools`
  ADD CONSTRAINT `fk_tools_types1` FOREIGN KEY (`type_id`) REFERENCES `types` (`type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Begrensninger for tabell `types`
--
ALTER TABLE `types`
  ADD CONSTRAINT `fk_types_categories1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
