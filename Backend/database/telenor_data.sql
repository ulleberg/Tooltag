-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 10. Aug, 2018 15:09 PM
-- Server-versjon: 10.1.26-MariaDB-0+deb9u1
-- PHP Version: 7.0.30-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `telenor`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `cows`
--

CREATE TABLE `cows` (
  `cow_id` int(11) NOT NULL,
  `cow_name` varchar(45) DEFAULT NULL,
  `note` varchar(500) DEFAULT NULL,
  `in_service` tinyint(1) DEFAULT NULL,
  `purchased_date` datetime DEFAULT NULL,
  `service_date` datetime DEFAULT NULL,
  `in_transit` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `cows`
--

INSERT INTO `cows` (`cow_id`, `cow_name`, `note`, `in_service`, `purchased_date`, `service_date`, `in_transit`) VALUES
(1, 'COW5', 'Antenne må skiftes.', 1, '2016-02-17 00:00:00', '2019-10-27 00:00:00', 0),
(2, 'COW6', 'Skal til O-Festivalen 22.08. Kontaktperson Olav Lundanes +47 48 34 45 34', 1, '2014-02-17 00:00:00', '2019-02-17 00:00:00', 1),
(3, 'COW22', 'Her kan du skrive et langt notat', 1, '2013-02-17 00:00:00', '2019-02-17 00:00:00', 0),
(4, 'COW10', 'Batteri må skiftes.\r\n\r\nMulig lekkasje i taket som må undersøkes ved neste service.,', 1, '2014-02-21 00:00:00', '2018-11-17 00:00:00', 0),
(5, 'COW12', 'gva ghgj', 1, '2014-05-08 00:00:00', '2020-02-17 00:00:00', 1),
(6, 'COW30', NULL, 0, '2010-02-10 00:00:00', NULL, 0),
(7, 'COW24', 'Må sendes til reparasjon umiddelbart. Feil ved strømforsyning.', 1, '2014-02-17 00:00:00', '2020-02-17 00:00:00', 0),
(8, 'COW 17', 'None', 0, '2010-02-10 00:00:00', NULL, 0),
(9, 'COW26', 'Alt i orden.', 1, '2014-02-17 00:00:00', '2021-02-17 00:00:00', 0),
(10, 'COW13', 'Har sett bedre dager. ', 0, '2000-02-17 00:00:00', NULL, 0);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `data`
--

CREATE TABLE `data` (
  `cow_id` int(11) NOT NULL,
  `long` double DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `temp` double DEFAULT NULL,
  `antenna_direction` double DEFAULT NULL,
  `power_on` tinyint(1) DEFAULT NULL,
  `door_open` tinyint(1) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dataark for tabell `data`
--

INSERT INTO `data` (`cow_id`, `long`, `lat`, `temp`, `antenna_direction`, `power_on`, `door_open`, `timestamp`) VALUES
(1, 5.897, 59.611, 19.999, 344, 1, 0, '2018-08-07 08:30:06'),
(2, 6.677, 57.586, 11.076, 44, 1, 0, '2018-08-07 08:30:06'),
(3, 16.2, 67, 15.338, 194, 1, 1, '2018-08-07 08:30:06'),
(4, 11.2, 60, 14.591, 77, 1, 0, '2018-08-07 08:30:06'),
(5, 11.5, 61, 18.284, 288, 0, 0, '2018-08-07 08:30:06'),
(6, 7.359, 62.66, 16.218, 18, 0, 1, '2018-08-07 08:30:06'),
(7, 29, 70, 14.335, 224, 0, 0, '2018-08-07 08:30:06'),
(8, 8.58, 62.711, 13.302, 111, 1, 1, '2018-08-07 08:30:06'),
(9, 11.5, 63, 14.936, 10, 0, 0, '2018-08-07 08:30:06'),
(10, 10.156, 62.14, 13.493, 239, 0, 0, '2018-08-07 08:30:06'),
(1, 6.844, 58.374, 18.78, 267, 1, 0, '2018-08-07 08:39:40'),
(2, 10.583, 61.839, 11.307, 91, 1, 0, '2018-08-07 08:39:40'),
(3, 16.2, 67, 16.647, 0, 1, 0, '2018-08-07 08:39:40'),
(4, 11.2, 60, 14.461, 329, 0, 1, '2018-08-07 08:39:40'),
(5, 11.5, 61, 12.572, 333, 1, 0, '2018-08-07 08:39:40'),
(6, 8.92, 58.555, 11.821, 227, 1, 1, '2018-08-07 08:39:40'),
(7, 29, 70, 17.275, 106, 0, 0, '2018-08-07 08:39:40'),
(8, 12.121, 63.477, 11.705, 5, 0, 1, '2018-08-07 08:39:40'),
(9, 11.5, 63, 10.073, 358, 1, 1, '2018-08-07 08:39:40'),
(10, 7.487, 61.968, 10.163, 108, 1, 0, '2018-08-07 08:39:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cows`
--
ALTER TABLE `cows`
  ADD PRIMARY KEY (`cow_id`);

--
-- Indexes for table `data`
--
ALTER TABLE `data`
  ADD KEY `fk_data_cows` (`cow_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cows`
--
ALTER TABLE `cows`
  MODIFY `cow_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `data`
--
ALTER TABLE `data`
  ADD CONSTRAINT `fk_data_cows` FOREIGN KEY (`cow_id`) REFERENCES `cows` (`cow_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
