import pymysql
from time import localtime, strftime, strptime
from random import randint

db = pymysql.connect("localhost", "demouser", "demopw", "demo")
cursor = db.cursor()


def get_project_overview(is_finished):
	sql = """SELECT projects.project_id, projects.name, projects.start_date, projects.finish_date
	FROM projects
	WHERE projects.is_finished = {};""".format(is_finished)
	cursor.execute(sql)
	data = list(cursor.fetchall())
	data = [list(project) for project in data]
	for project in data:
		start_time = project[2]
		finish_time = project[3]
		days_left = (finish_time - start_time).days
		project[2] = days_left
		project.pop(3)
	return data



def get_project_info(project_id):
	sql = """
	SELECT projects.name, projects.start_date, projects.finish_date,
	projects.note, contacts.name, contacts.phone, contacts.email,
	addresses.street, addresses.postal_code, projects.is_finished,
	addresses.long, addresses.lat, projects.radius,
	projects.address_id, projects.contact_id
	FROM projects
	INNER JOIN addresses ON projects.address_id = addresses.address_id
	INNER JOIN contacts ON projects.contact_id = contacts.contact_id
	WHERE projects.project_id = {};
	""".format(project_id)

	cursor.execute(sql)
	data = cursor.fetchone()
	return data



def get_project_tools(project_id):
	sql = """
	SELECT tools.tool_id, tools.tool_name, categories.category_name, types.type_name, a.last_seen, tools.note
	FROM measurements AS a
	INNER JOIN (
    	SELECT tool_id, MAX(last_seen) AS ls
    	FROM measurements
    	GROUP BY tool_id
		) AS b ON a.tool_id = b.tool_id AND a.last_seen = ls
	INNER JOIN tools ON tools.tool_id = a.tool_id
	INNER JOIN types ON types.type_id = tools.type_id
	INNER JOIN categories ON categories.category_id = types.category_id
	WHERE a.project_id = {}
	ORDER BY categories.category_name ASC, types.type_name ASC;
	""".format(project_id)

	cursor.execute(sql)
	data = cursor.fetchall()
	return data



def get_categories():
	sql = "SELECT categories.category_id, categories.category_name FROM categories;"
	cursor.execute(sql)
	data = cursor.fetchall()
	return data


def get_categories_and_types():
	sql = """
	SELECT types.category_id, categories.category_name, types.type_id, types.type_name
	FROM types
	INNER JOIN categories ON categories.category_id = types.category_id;
	"""
	cursor.execute(sql)
	data = cursor.fetchall()
	return data


def get_types_by_category(category_id):
	sql = """
	SELECT types.type_id, types.type_name
	FROM types
	WHERE category_id = {};
	""".format(category_id)
	cursor.execute(sql)
	data = cursor.fetchall()
	return data




def get_tools_by_category(category_id):
	sql = """
	SELECT tools.tool_id, tools.tool_name, types.type_name, tools.manufacturer, tools.purchased_date, tools.service_date, a.last_seen, projects.name, tools.note
	FROM types
	INNER JOIN tools ON tools.type_id = types.type_id
	INNER JOIN measurements AS a ON tools.tool_id = a.tool_id
	INNER JOIN (
    	SELECT tool_id, MAX(last_seen) AS ls
    	FROM measurements
    	GROUP BY tool_id
	) AS b ON a.tool_id = b.tool_id AND a.last_seen = b.ls
	INNER JOIN projects ON a.project_id = projects.project_id
	WHERE types.category_id = {}
	ORDER BY types.type_name ASC, tools.tool_name ASC;
	""".format(category_id)

	cursor.execute(sql)
	data = cursor.fetchall()
	return data



def get_tool_history(tool_id):
	sql = """
	SELECT projects.name, addresses.place_name, addresses.street, measurements.last_seen
	FROM measurements
	INNER JOIN projects ON projects.project_id = measurements.project_id
	INNER JOIN addresses ON addresses.address_id = projects.address_id
	WHERE measurements.tool_id = {}
	ORDER BY measurements.last_seen ASC;
	""".format(tool_id)

	cursor.execute(sql)
	data = cursor.fetchall()
	return data



def get_type_count_by_category_on_location(category_id, project_id):
	sql = """
	SELECT COUNT(types.type_id) AS type_count, types.type_id, types.type_name
	FROM types
	INNER JOIN tools ON tools.type_id = types.type_id
	INNER JOIN measurements AS a ON a.tool_id = tools.tool_id
	INNER JOIN (
    	SELECT tool_id, MAX(last_seen) AS ls
    	FROM measurements
    	GROUP BY tool_id
	) AS b ON a.tool_id = b.tool_id AND a.last_seen = b.ls
	WHERE types.category_id = {} AND a.project_id = {}
	GROUP BY  types.type_name
	ORDER BY type_count DESC;
	""".format(category_id, project_id)

	cursor.execute(sql)
	data = cursor.fetchall()
	return data

def get_tool_count_on_all_locations(type_id):
	sql = """
	SELECT COUNT(tools.type_id) AS type_count, projects.name
	FROM tools
	INNER JOIN measurements AS a ON a.tool_id = tools.tool_id
	INNER JOIN (
    	SELECT tool_id, MAX(last_seen) AS ls
    	FROM measurements
    	GROUP BY tool_id
	) AS b ON a.tool_id = b.tool_id AND a.last_seen = b.ls
	INNER JOIN projects ON projects.project_id = a.project_id
	WHERE tools.type_id = {}
	GROUP BY projects.name
	ORDER BY type_count DESC;
	""".format(type_id)

	cursor.execute(sql)
	data = cursor.fetchall()
	return data


def new_tool(type_id, tool_name, manufacturer, note, service_date, purchased_date, uid): #dates must be in format YYYY-MM-DD.
	sql = """
	INSERT INTO tools (tools.type_id, tools.tool_name, tools.manufacturer, tools.note, tools.service_date, tools.purchased_date, tools.uid)
	VALUES ({}, '{}', '{}', '{}', '{}', '{}', {});
	""".format(type_id, tool_name, manufacturer, note, service_date, purchased_date, uid)
	try:
		cursor.execute(sql)
		db.commit()
		print(sql)
	except:
		db.rollback()


def get_tool_info(tool_id):
	sql = """
	SELECT tools.type_id, tools.tool_name, tools.manufacturer, tools.note, tools.service_date, tools.purchased_date
	FROM tools
	WHERE tools.tool_id = {};
	""".format(tool_id)
	cursor.execute(sql)
	data = cursor.fetchone()
	return data

def get_tool_extra_info(tool_id):
	sql = """
	SELECT tools.type_id, types.type_name, categories.category_name, categories.category_id, tools.rssi, tools.uid, 
	tools.tool_name, tools.manufacturer, tools.note, tools.service_date, tools.purchased_date
	FROM tools 
	INNER JOIN types ON types.type_id = tools.type_id
	INNER JOIN categories ON categories.category_id = types.category_id
	WHERE tools.tool_id = {};""".format(tool_id)
	cursor.execute(sql)
	data = cursor.fetchone()
	res = dict(
		type_id = data[0], 
		type_name = data[1], 
		category_name = data[2], 
		category_id=data[3], 
		rssi =data[4], 
		uid = data[5],
		name = data[6],
		manufacturer = data[7],
		note = data[8],
		service_date = data[9],
		purchased_date = data[10],
		)
	return res


def update_tool(tool_id, type_id, tool_name, manufacturer, note, service_date, purchased_date):
	sql = """
	UPDATE tools
	SET tools.type_id = {}, tools.tool_name = '{}', tools.manufacturer = '{}', tools.note = '{}', tools.service_date = '{}', tools.purchased_date = '{}'
	WHERE tools.tool_id = {};
	""".format(type_id, tool_name, manufacturer, note, service_date, purchased_date, tool_id)
	try:
		cursor.execute(sql)
		db.commit()
	except:
		db.rollback()

def update_address(address_id, street, postal_code, lng, lat, place_name):
	sql = """
	UPDATE addresses
	SET addresses.street = '{}', addresses.postal_code = {}, addresses.long = {}, addresses.lat = {}, addresses.place_name = '{}'
	WHERE addresses.address_id = {};
	""".format(street, postal_code, lng, lat, place_name, address_id)
	try:
		cursor.execute(sql)
		db.commit()
	except:
		db.rollback()


def new_project(contact_id, name, start_date, finish_date, note, address_id):
	sql = """
	INSERT INTO projects (projects.contact_id, projects.name, projects.start_date, projects.finish_date, projects.note, projects.address_id)
	VALUES ({}, '{}', {}, {}, '{}', {});
	""".format(contact_id, name, start_date, finish_date, note, address_id)
	print(sql)
	try:
		cursor.execute(sql)
		db.commit()
	except:
		db.rollback()


def update_project(project_id, contact_id, name, start_date, finish_date, note, address_id, is_finished, radius):
	sql = """
	UPDATE projects
	SET projects.contact_id = {}, projects.name = '{}', projects.start_date = '{}', projects.finish_date = '{}', projects.note = '{}', projects.address_id = {}, projects.is_finished ={}, projects.radius = {}
	WHERE projects.project_id = {}
	""".format(contact_id, name, start_date, finish_date, note, address_id, is_finished, radius, project_id)
	try:
		cursor.execute(sql)
		db.commit()
	except:
		db.rollback()



def get_contacts():
	sql = """
	SELECT contacts.contact_id, contacts.name, contacts.phone, contacts.email
	FROM contacts
	"""
	cursor.execute(sql)
	data = cursor.fetchall()
	return data


def new_contact(name, phone, email):
	sql = """
	INSERT INTO contacts (contacts.name, contacts.phone, contacts.email)
	VALUES ('{}', '{}', '{}')
	""".format(name, str(phone), email)
	try:
		cursor.execute(sql)
		db.commit()
	except:
		db.rollback()

def update_contact(contact_id, name, phone, email):
	sql = """
	UPDATE contacts
	SET contacts.name = '{}', contacts.phone = '{}', contacts.email = '{}'
	WHERE contacts.contact_id = {}
	""".format(name, str(phone), email, contact_id)
	try:
		cursor.execute(sql)
		db.commit()
	except:
		db.rollback()

def update_measurements(tool_id, project_id, gateway_id):
	sql = """
	SELECT a.tool_id, a.project_id, a.gateway_id, a.last_seen
	FROM measurements AS a
	INNER JOIN (
    	SELECT tool_id, MAX(last_seen) as ls
    	FROM measurements
    	GROUP BY tool_id
	) AS b WHERE a.tool_id = b.tool_id AND a.last_seen = b.ls
	AND a.tool_id = {} AND a.project_id = {}
	""".format(tool_id, project_id)

	cursor.execute(sql)
	data = cursor.fetchone()
	if data == None: # no matching record found, make new
		sql = """
		INSERT INTO measurements (measurements.tool_id, measurements.project_id, measurements.gateway_id, measurements.last_seen)
		VALUES ({}, {}, {}, NOW());
		""".format(tool_id, project_id, gateway_id)
		try:
			cursor.execute(sql)
			db.commit()
		except:
			db.rollback()
	else: # update timestamp and gateway
		last_seen = data[3]
		sql = """
		UPDATE measurements
		SET measurements.gateway_id = {}, measurements.last_seen = NOW()
		WHERE measurements.tool_id = {} AND measurements.last_seen = '{}';
		""".format(gateway_id, tool_id, last_seen)
		try:
			cursor.execute(sql)
			db.commit()
		except:
			db.rollback()


def generate_measurements(number_of_tools):
	for tool in range(1,number_of_tools+1):
		gateway = randint(1,5)
		project = randint(1,5)
		sql = """
		INSERT INTO measurements (tool_id, gateway_id, project_id, last_seen)
		VALUES ({},{},{},NOW())
		""".format(tool, gateway, project)
		try:
			cursor.execute(sql)
			db.commit()
		except:
			db.rollback()

def get_gateways():
	sql = """SELECT gateways.gateway_id, gateways.gateway_uid, gateways.gateway_name, gateways.long, gateways.lat FROM gateways"""
	cursor.execute(sql)
	data = cursor.fetchall()
	
	res = []
	for d in data:
		res.append(dict(gateway_id = d[0], gateway_uid = d[1], gateway_name = d[2], lng = d[3], lat = d[4] ))

	return res



if __name__ == "__main__":
	print(get_tool_extra_info(25))
