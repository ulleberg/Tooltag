import pymysql
from time import localtime, strftime, strptime
from random import randint

db = pymysql.connect("tooltag.no", "telenoruser", "demopw", "telenor", cursorclass=pymysql.cursors.DictCursor)
cursor = db.cursor()

# Generate sensor data for each COW
def generate_data(number_of_cows):
	for cow_id in range(1,number_of_cows+1):
		long = randint(4783,12310) / 1000
		lat = randint(57000,64500)  / 1000
		temp = randint(10000, 20000) / 1000
		antenna_direction = randint(0, 360)
		power_on = randint(0,1)
		door_open = randint(0, 1)

		sql = """
		INSERT INTO data (data.cow_id, data.long, data.lat, data.temp, data.antenna_direction, data.power_on, data.door_open)
		VALUES ({},{},{},{},{},{},{})
		""".format(cow_id, long, lat, temp, antenna_direction, power_on, door_open)
		try:
			cursor.execute(sql)
			db.commit()
			print('Query complete.', )
		except pymysql.Error as e:
			print('Query error', e, e.args[0], sql)
			db.rollback()


def get_cow_overview():
	sql = """
	SELECT cows.cow_id, cows.cow_name, data.long, data.lat, cows.in_service, cows.in_transit, data.antenna_direction
	FROM cows
	INNER JOIN data ON cows.cow_id = data.cow_id
	INNER JOIN (
    	SELECT cow_id, MAX(timestamp) AS max_timestamp
    	FROM data
    	GROUP BY cow_id
	) AS sorted_data ON data.cow_id = sorted_data.cow_id AND data.timestamp = sorted_data.max_timestamp
	"""
	try:
		cursor.execute(sql)
		db.commit()
		data = cursor.fetchall()
		return data
	except:
		db.rollback()


def get_cow_info(cow_id):
	sql = """
<<<<<<< HEAD
	SELECT cows.cow_name, cows.note, cows.in_service, cows.purchased_date, cows.service_date, cows.in_transit, data.temp, data.antenna_direction, data.power_on, data.door_open, data.timestamp
=======
	SELECT cows.cow_id, cows.cow_name, data.long, data.lat, cows.note, cows.in_service, cows.purchased_date, cows.service_date, cows.in_transit, data.temp, data.antenna_direction, data.power_on, data.door_open, data.timestamp
>>>>>>> frontend_dev
	FROM cows
	INNER JOIN data ON cows.cow_id = data.cow_id
	INNER JOIN (
    	SELECT cow_id, MAX(timestamp) AS max_timestamp
    	FROM data
    	GROUP BY cow_id
	) AS sorted_data ON data.cow_id = sorted_data.cow_id AND data.timestamp = sorted_data.max_timestamp
	WHERE cows.cow_id = {};
	""".format(cow_id)
	try:
		cursor.execute(sql)
		db.commit()
		data = cursor.fetchone()
		return data
	except:
		db.rollback()
<<<<<<< HEAD
=======


# Update the note of a speccific COW.
def update_cow_note(cow_id, note):
	sql = """
	UPDATE cows
	SET cows.note = '{}'
	WHERE cow_id = {};
	""".format(note, cow_id)
	try:
		cursor.execute(sql)
		db.commit()
	except:
		db.rollback()

def update_position(cow_id, long, lat):
	sql = """
	UPDATE data
	SET data.long = {}, data.lat = {}
	WHERE data.cow_id = {};
	""".format(long, lat, cow_id)
	try:
		cursor.execute(sql)
		db.commit()
	except:
		db.rollback()

if 1:
	''' new positions '''
	update_position(3, 16.2,67)
	update_position(4, 11.2,60)
	update_position(5, 11.5,61)
	update_position(7, 29,70)
	update_position(9, 11.5,63)
>>>>>>> frontend_dev
