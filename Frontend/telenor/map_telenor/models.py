from django.db import models

class Cow(models.Model):
    cow_id = models.IntegerField(default=0)
    cow_name = models.CharField(max_length=200)
    notes = models.CharField(max_length=200)
    purchase_date = models.DateField('date purchased')
    service_date = models.DateField('date service')
    status_active = models.IntegerField(default=1)

    def __str__(self):
        return self.cow_name


class Sensor(models.Model):
    cow = models.ForeignKey(Cow, on_delete=models.CASCADE)
    sensor_type = models.CharField(max_length=200)
    sensor_value = models.IntegerField(default=0)
    time_stamp = models.DateTimeField('Last seen')

    def __str__(self):
        return self.sensor_type
