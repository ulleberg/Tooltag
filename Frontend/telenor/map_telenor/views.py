
from django.shortcuts import render

from django.http import HttpResponse

from .telenor_queries import *

#This is temporary. The only view.

def cow_overview(request):
	data = get_cow_overview()
	return HttpResponse(data)

def cow_info(request, cow_id):
	data = get_cow_info(cow_id)
	return HttpResponse(data)



def index(request):
	return cow_overview(request)

def cow_overview(request): return render(request,'map_telenor/map.html', {'herd' :[get_cow_info(cow['cow_id']) for cow in get_cow_overview()]})



	#changing the project note and leave the rest unchanged

def cow_update_note(request, cow_id):
	new_note  = request.GET['note']
	update_cow_note(cow_id, new_note)
	return cow_overview(request)
