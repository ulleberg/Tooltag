from .queries import *

from .models import *
from django.views.generic import TemplateView
from .forms import TestForm


from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime





def index(request):
	"""Redirects to the project-overview site -'construction_yard.html' when
	typing byggmesteran.tooltag.no/

    Args:
        request (HttpRequest):

    Returns:
        HttpResponse :  A HttpResponse object a django object which django use to render webpage
    """

	return construction_yard(request, 0)


def construction_yard(request, finished):
	"""Render the "main-page" which shows a overview of all the projects
	which statisfy project.is_finshed == finished

    Args:
		request (HttpRequest):
		finished (bool)  :

    Returns:
        HttpResponse :  A HttpResponse object a django object which django use to render webpage
    """

	#a list of list, where the elements in the innerlist is the project attributes.
	project_overview = get_project_overview(finished)

	#removes the warehouse
	project_overview.pop(0) #Yeah the warehouse/"lager" is hardcoded as the first element (this may lead to some problems in the futere)

	#A dictionary of value, where the keys are used in the HTML and are referring to the dict-values
	context = {
		'Company_name'    : 'Byggmesteran',
		'project_overview': project_overview,
		'finished'        : finished
		}
	return render(request, 'byggmesteran/construction_yard.html', context)

def warehouse(request):
	"""Render the "lager"-page, which shows details about the "lager"

    Args:
        request (HttpRequest):

    Returns:
        HttpResponse :  A HttpResponse object a django object which django use to render webpage
    """
	item_table = get_project_tools(1)# a list of tools registered in project 1 (lager'et)
	project_info = get_project_info(1)# general information  for the "lager"
	#NOT IF THE WAREHOUSE ID CHANGE FROM 1, WE ARE IN TROUBLE... :)

	#A dictionary of value, where the keys are used in the HTML and are referring to the dict-values
	context = {
		'Company_name'            : 'Byggmesteran',
		'item_table_header'       : ('Navn', 'Kategori', 'Type', 'Sist registrert', 'Merknad'),
		'item_table'              : item_table,
		'warehouse_name'          : project_info[0],
		'warehouse_start_date'    : project_info[1].strftime('%d-%m-%Y'),
		'warehouse_end_date'      : project_info[2].strftime('%d-%m-%Y'),
		'warehouse_note'          : project_info[3],
		'warehouse_contact_name'  : project_info[4],
		'warehouse_contact_number': project_info[5],
		'warehouse_email'         : project_info[6],
		'warehouse_street'        : project_info[7],
		'warehouse_postal_code'   : project_info[8],
		'warehouse_id'            : 1
		}
	#hardcoded indices :)

	return render(request, 'byggmesteran/warehouse.html', context)


def construction_yard_details(request, project_id):
	"""Render a webpage whith detailes about the selected project

    Args:
		request (HttpRequest):
		project_id (int)     :  The project_id of the desired project

    Returns:
        HttpResponse :  A HttpResponse object a django object which django use to render webpage
    """

	item_table = list(get_project_tools(project_id))#list of tools in the project
	for i in range(len(item_table)):
		item_table[i] = list(item_table[i])
		item_table[i][4] = item_table[i][4].strftime('%d-%m-%Y. %H:%M')#formating the time

	project_info = get_project_info(project_id)# general information for the desired project

	#A dictionary of value, where the keys are used in the HTML and are referring to the dict-values
	context = {
		'Company_name'       : 'Byggmesteran',

		'item_table_header'     : ('Navn', 'Kategori', 'Type', 'Sist registrert', 'Merknad'),
		'item_table'            : item_table,
		'project_name'          : project_info.name,
		'project_note'          : project_info.note,
		'project_start_date'    : project_info.start_date.strftime('%d-%m-%Y'),
		'project_end_date'      : project_info.finish_date.strftime('%d-%m-%Y'),
		'project_street'        : project_info.address_street, #unused
		'project_postal_code'   : project_info.address_postal_code,#unused
		'project_is_finished'   : project_info.is_finished,
		'project_contact_name'  : project_info.contact_name,
		'project_contact_number': project_info.contact_phone,
		'project_email'         : project_info.contact_email,
		'long'                  : project_info.long,
		'lat'                   : project_info.lat,
		'project_radius'        : project_info.radius, #unused
		'project_address_id'    : project_info.address_id,
		'project_contact_id'    : project_info.contact_id,
		'project_id'            : project_id,
		'positions'             : [project_info.long, project_info.lat],

		}

	return render(request, 'byggmesteran/construction_yard_details.html', context)


def construction_yard_details_edit(request, creating_new_project, project_id):
	"""Render a webpage in which the user can create a new project or edit an already existing project

    Args:
		request (HttpRequest)      :
		creating_new_project (bool): Indicates whether the user is creating a new project or not(editing an existing)
		project_id (int)           : The id of the project which a user is going to edit, a 'DON'T CARE' if creating_new_project == True

    Returns:
        HttpResponse :  A HttpResponse object a django object which django use to render webpage
    """

	#a list of list, where the elements in the innerlist is the project attributes.

	contacts = get_contacts() # alist of all contacts

	if creating_new_project:#a user is creating a new project

		#A dictionary of value, where the keys are used in the HTML and are referring to the dict-values
		context = {
			'caption'          		: 'Nytt prosjekt',
			'contacts'              : contacts,
			'long'                  : 11.4709864,
			'lat'                   : 63.7922438,
			'project_start_date'    : datetime.now().strftime('%Y-%m-%d'),
			'project_end_date'      : datetime.now().strftime('%Y-%m-%d'),
			'new_project'			: True,

			#by default we set the contact as the first contact in the list of all contacts
			'project_contact_name'  : contacts[0].name,
			'project_contact_number': contacts[0].phone,
			'project_email'         : contacts[0].email,
			'project_contact_id'    : contacts[0].contact_id,

		}

		return render(request, 'byggmesteran/construction_yard_details_edit.html', context)

	project_info = get_project_info(project_id)#project info

	#A dictionary of value, where the keys are used in the HTML and are referring to the dict-values
	context = {
		'Company_name'          : 'Byggmesteran',
		'caption'          		: 'Rediger prosjekt',
		'contacts'              : contacts,
		'project_name'          : project_info.name,
		'project_start_date'    : project_info.start_date.strftime('%Y-%m-%d'),
		'project_end_date'      : project_info.finish_date.strftime('%Y-%m-%d'),
		'project_note'          : project_info.note,
		'project_contact_name'  : project_info.contact_name,
		'project_contact_number': project_info.contact_phone,
		'project_email'         : project_info.contact_email,
		'long'                  : project_info.long,
		'lat'                   : project_info.lat,
		'project_radius'        : project_info.radius,
		'project_contact_id'    : project_info.contact_id,
		'project_id'            : project_id,
		}

	return render(request, 'byggmesteran/construction_yard_details_edit.html', context)




def tool(request, category_id):
	"""Render a webpage which contains all information for all the tools in the system

	NB! category_id is a don't care

    Args:
		request (HttpRequest) :
		category_id (int) : DONT CARE // old functionallity

    Returns:
        HttpResponse :  A HttpResponse object a django object which django use to render webpage
    """

	#a list of list, where the elements in the innerlist is the project attributes.
	categories = get_categories()
	#tools.tool_id, tools.tool_name, types.type_name, tools.manufacturer, tools.purchased_date, tools.service_date, a.last_seen, projects.name, tools.note
	types_by_category = [get_types_by_category(c[0]) for c in categories ]

	#creating all list of all tools that exist, and formating it
	tool_all = []
	for c in get_categories():
		tool_cat = get_tools_by_category(c[0])
		tool_cat_edit = []
		for tool in tool_cat:
			it = list(tool)
			it[4] = it[4].strftime('%d-%m-%Y') if it[4] is not None else it[4]
			it[5] = it[5].strftime('%d-%m-%Y') if it[5] is not None else it[5]
			it[6] = it[6].strftime('%d-%m-%Y. %H:%M') if it[6] is not None else it[6]
			tool_cat_edit.append(it)
		tool_all.append(tool_cat_edit)


	#A dictionary of value, where the keys are used in the HTML and are referring to the dict-values
	context = {
		'item_table_header': ('Navn', 'Type', 'Merke', 'Innkj.dato', 'Servicedato', 'Sist tid registrert', 'Sist sted registrert', 'Merknad'),
		'Company_name'     : 'Byggmesteran',
		'categories'       : categories,
		'types_by_category': types_by_category,
		'tool_all'         : tool_all,
		}

	return render(request, 'byggmesteran/tool.html', context)



def contacts(request):
	"""Render a webpage which contains all the contacts that exist

    Args:
		request (HttpRequest) :

    Returns:
        HttpResponse :  A HttpResponse object a django object which django use to render webpage
    """
	context = {
		'item_table_header': ('Navn','telefon', 'email'),
		'Company_name'     : 'Byggmesteran',
		'contacts'         : [ c for c in get_contacts() ],
	}
	return render(request, 'byggmesteran/contacts.html', context)

def contact_edit(request, new_contact, contact_id):
	"""Render a webpage in which the user can create a new contact or edit an already existing contact

    Args:
		request (HttpRequest) :
		new_contact (bool): Indicates whether the user is creating a new contact or not(editing an existing)
		project_id (int) : The id of the contact which a user is going to edit, a 'DON'T CARE' if creating_new_project == True

    Returns:
        HttpResponse :  A HttpResponse object a django object which django use to render webpage
    """

	#a list of list, where the elements in the innerlist is the project attributes.

	context= {
		'Company_name'  : 'Byggmesteran',
		'contact_name'  : "",
		'contact_number': "",
		'contact_email' : "olanordman@stud.ntnu.no",
		'contact_id'    : 1,
	}


	if new_contact:
		context["add_new_contact"] = True #the HTML needs this information,
	else:
		contact_info = list(filter(lambda contact: contact[0] == contact_id, get_contacts()))[0]
		#if we are going to edit a contact, we will display the current name, phone-number, email address etc...
		context['contact_name']   = contact_info[1]
		context['contact_number'] =  contact_info[2]
		context['contact_email' ] = contact_info[3]
		context['contact_id']     = contact_id

	return render(request, 'byggmesteran/contact_edit.html', context)



def contact_update(request, new_contact_added, contact_id):
	contact_name   = request.GET['contact_name']
	contact_email  = request.GET['contact_email']
	contact_number = request.GET['contact_number']
	if new_contact_added:
		new_contact(contact_name, contact_number, contact_email)
		index(request)
	else:
		update_contact(contact_id, contact_name, contact_number, contact_email)
	return contacts(request)



def counstruction_edit_update_project(request, creating_new_project, project_id):

	project_name         = request.POST['project_name']
	project_start_date   = request.POST['project_start_date']
	project_end_date     = request.POST['project_end_date']
	project_note         = request.POST['project_note']
	lng                  = float(request.POST['long'])
	lat                  = float(request.POST['lat'])


	#check if we have pressed the "save-project" button
	if request.POST.get("main_save"):
		contact_id = int(request.POST["sample"])

		#if the we are going to create a new project
		if creating_new_project:
			address_id = new_address(lng, lat)#we must creat a new address

			#creating a new project
			project_id = new_project(contact_id, project_name, project_start_date, project_end_date, project_note, address_id, False, 200)#hard coded is_finish and radius
			return construction_yard_details(request, project_id)#loading the detail site for the new project

		#if we are going to edit the project
		else:

			p =  get_project_info(project_id)

			#we must update new coordinates
			update_address(p.address_id, p.address_street, p.address_postal_code, lng, lat, "<not implemented>") #TODO

			#update project
			update_project(project_id,contact_id, project_name, project_start_date, project_end_date, project_note,  p.address_id, p.is_finished, p.radius)
			return construction_yard_details(request, project_id)#loading the detail site for the new project

	#if not request.POST.get("main_save") that means that we are creating a new contact inside the modal. and we want to refresh construction_yard_details.html
	#with a new contact and sace the changes
	else:
		contact_name   = request.POST['contact_name']
		contact_email  = request.POST['contact_email']
		contact_number = request.POST['contact_number']
		contact_id = new_contact(contact_name, contact_number, contact_email)
		context = {
			'Company_name'          : 'Byggmesteran',
			'contacts'              : get_contacts(),
			'project_name'          : project_name,
			'project_start_date'    : project_start_date,
			'project_end_date'      : project_end_date,
			'project_note'          : project_note,
			'project_contact_name'  : contact_name,
			'project_contact_number': contact_number,
			'project_email'         : contact_email,
			'long'                  : lng,
			'lat'                   : lat,
			'project_radius'        : 200, #TODO fiks hardkode
			'project_contact_id'    : contact_id,
			'project_id'            : project_id,
			'new_project'			: creating_new_project,
		}

		return render(request, 'byggmesteran/construction_yard_details_edit.html', context)



def set_project_status(request, project_id, value):
	"""changing the project status and leave the rest unchanged"""
	p = get_project_info(project_id)
	update_project(project_id, p.contact_id, p.name, p.start_date, p.finish_date, p.note, p.address_id, value, p.radius)
	return construction_yard_details(request, project_id)

def project_update_note(request, project_id):
	"""changing the project note and leave the rest unchanged"""
	new_note  = request.GET['project_note']
	p = get_project_info(project_id)
	update_project(project_id, p.contact_id, p.name, p.start_date, p.finish_date, new_note, p.address_id, p.is_finished, p.radius)

	return construction_yard_details(request, project_id)


def overall_map(request, gateway_selected, project_selected):
	"""Render a map showing gateways and project when the corresponding args are flagged


    Args:
        request (HttpRequest):
        gateway_selected (bool): showing gateways
        project_selected (bool): showing projects

    Info:
    	If both gateway_selected are True and project_selected are True, both gatways and projects
    	will be shown on the map

    Returns:
        HttpResponse :  A HttpResponse object a django object which django use to render webpage
    """


	data = []#declearing a list of dictonaries, containing all the necessary information
	         #about all the projects and gateways

	#appending gateway- informations as dictonary to data
	if gateway_selected:
		for gateway in get_gateways():
			data.append(dict(
							lng   = gateway.lng,
							lat   = gateway.lat,
							name  = gateway.gateway_name,
							type  = "gateway",
							id    = gateway.gateway_id,
							tools = get_gateway_tools(gateway.gateway_id)
							))

	#appending project- informations as dictonary to data
	if project_selected:
		for project_overview in get_project_overview(0)[1:]:#dropper lager som er index 0/1
			project_id = project_overview[0]# get project id
			project = get_project_info(project_id)#get info about the projecet
			data.append(dict(lng = project.long, lat = project.lat, name = project.name, type = "project", id = project_id))

	context = {
			'gateway_selected' : gateway_selected,
			'project_selected' : project_selected,
			'data'             : data,
			'item_table_header': ('Navn', 'Kategori', 'Type', 'Sist registrert', 'Merknad'),
	}
	return render(request,'byggmesteran/overall_map.html',context)

def tool_edit(request, tool_id):
	#Render a form for selected tool-id in html for edit tool information
	categories = get_categories()
	tool_extra_info = get_tool_extra_info(tool_id)
	types_by_category = {}
	for c in categories:
		types_by_category[c[0]] = get_types_by_category(c[0])

	context = {
		'tool_id'            : tool_id,
		'tool_name'          : tool_extra_info["name"],
		'tool_manufacturer'  : tool_extra_info["manufacturer"],
		'tool_category_name' : tool_extra_info["category_name"],
		'tool_category_id'   : tool_extra_info["category_id"],
		'tool_type_name'     : tool_extra_info["type_name"],
		'tool_type_id'       : tool_extra_info["type_id"],
		'tool_note'          : tool_extra_info["note"],
		'tool_purchased_date': tool_extra_info['purchased_date'].strftime('%Y-%m-%d'),
		'tool_service_date'  : tool_extra_info['service_date'].strftime('%Y-%m-%d'),
		'types_by_category'  : types_by_category,
		'categories'         : categories,
	}
	return render(request, 'byggmesteran/tool_edit.html', context)

def tool_edit_update(request, tool_id):
	"""When user sumbit the form in "tool_edit", we will save the changes to the DB"""
	tool_type_id        = request.POST['type_select']
	tool_name           = request.POST['tool_name']
	tool_manufacturer   = request.POST['tool_manufacturer']
	tool_note           = request.POST['tool_note']
	tool_service_date   = request.POST['tool_service_date']
	tool_purchased_date = request.POST['tool_purchased_date']

	update_tool(tool_id, tool_type_id, tool_name, tool_manufacturer, tool_note, tool_service_date, tool_purchased_date)

	return tool(request, tool_id)

def construction_delete(request, project_id, is_finished):
	"""TODO make this acctually work"""
	delete_project(project_id)
	return construction_yard(request, is_finished)
