  // Initialize and add the map
      var marker;


      function save()
      {
        var value = marker.getPosition();
        document.getElementById("output").innerHTML = "value = " + value;
      }



      function initMap()
      {
        // The location of Uluru
        var uluru = {lat: {{ lat }}, lng: {{ long }} };
        document.getElementById('long').value = {{lat}};
        document.getElementById('lat').value = {{long}};
        // The map, centered at Uluru
        map = new google.maps.Map(document.getElementById('map'), {zoom: 8, center:uluru});


		var marker = new google.maps.Marker({
				position: uluru,
		        icon: 'http://maps.google.com/mapfiles/kml/paddle/grn-blank.png',
		   		map:map,
		    });



        function moveMarker(coords)
        {
          marker.setPosition(coords);
          document.getElementById('long').value = coords.lng();
          document.getElementById('lat').value = coords.lat();

        }



        //marker.addListener('click', function(){infoWindow.open(map, marker);});



        //listen for click on map

      google.maps.event.addListener(map, 'click',
        function(event)
        {
          moveMarker(event.latLng);
        }
      );
    }
