# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Adresses(models.Model):
    adress_id = models.AutoField(primary_key=True)
    street = models.CharField(max_length=45, blank=True, null=True)
    postal_code = models.IntegerField(blank=True, null=True)
    long = models.DecimalField(max_digits=10, decimal_places=7, blank=True, null=True)
    lat = models.DecimalField(max_digits=10, decimal_places=7, blank=True, null=True)
    place_name = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'adresses'


class Categories(models.Model):
    category_id = models.AutoField(primary_key=True)
    category_name = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'categories'


class Contacts(models.Model):
    contact_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=45, blank=True, null=True)
    phone = models.IntegerField()
    email = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'contacts'


class Gateways(models.Model):
    gateway_id = models.AutoField(primary_key=True)
    gateway_uid = models.IntegerField(blank=True, null=True)
    gateway_name = models.CharField(max_length=45, blank=True, null=True)
    long = models.DecimalField(max_digits=10, decimal_places=7, blank=True, null=True)
    lat = models.DecimalField(max_digits=10, decimal_places=7, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'gateways'


class Measurements(models.Model):
    tool = models.ForeignKey('Tools', models.DO_NOTHING)
    gateway = models.ForeignKey(Gateways, models.DO_NOTHING)
    project = models.ForeignKey('Projects', models.DO_NOTHING)
    last_seen = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'measurements'


class Projects(models.Model):
    project_id = models.AutoField(primary_key=True)
    contact = models.ForeignKey(Contacts, models.DO_NOTHING)
    name = models.CharField(max_length=45, blank=True, null=True)
    start_date = models.DateTimeField(blank=True, null=True)
    finish_date = models.DateTimeField(blank=True, null=True)
    note = models.CharField(max_length=400, blank=True, null=True)
    adress = models.ForeignKey(Adresses, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'projects'


class Tools(models.Model):
    tool_id = models.AutoField(primary_key=True)
    type = models.ForeignKey('Types', models.DO_NOTHING, blank=True, null=True)
    tool_name = models.CharField(max_length=45, blank=True, null=True)
    manufacturer = models.CharField(max_length=45, blank=True, null=True)
    note = models.CharField(max_length=200, blank=True, null=True)
    service_date = models.DateTimeField(blank=True, null=True)
    purchased_date = models.DateTimeField(blank=True, null=True)
    rssi = models.IntegerField(blank=True, null=True)
    uid = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tools'


class Types(models.Model):
    type_id = models.AutoField(primary_key=True)
    type_name = models.CharField(max_length=45, blank=True, null=True)
    category = models.ForeignKey(Categories, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'types'
