__author__ = 'volrath'

from django.urls import path
from . import views

app_name = 'byggmesteran'

urlpatterns = [
    path('', views.index, name = 'index'),
	path('construction_yard/<int:finished>/', views.construction_yard, name='construction_yard'),
    path('construction_yard_details/<int:project_id>/', views.construction_yard_details, name='construction_yard_details'),
    path('construction_yard/edit/<int:creating_new_project>/<int:project_id>/', views.construction_yard_details_edit, name='construction_yard_details_edit'),

    path('counstruction_edit_update_project/<int:creating_new_project>/<int:project_id>/', views.counstruction_edit_update_project, name ="counstruction_edit_update_project"),
    path('project_update_note/<int:project_id>/', views.project_update_note, name = "project_update_note"),
    path('set_project_status/<int:project_id>/<int:value>/', views.set_project_status, name = "set_project_status"),
    path('construction_delete/<int:project_id>/<int:is_finished>/', views.construction_delete, name = "construction_delete"),

    path('warehouse/', views.warehouse, name = 'warehouse'),

    path('overall_map/<int:gateway_selected>/<int:project_selected>/', views.overall_map, name = 'overall_map'),


    path('contacts/', views.contacts, name = 'contacts'),
    path('contact_edit/<int:new_contact>/<int:contact_id>', views.contact_edit, name = "contact_edit"),
    path('contact_update/<int:new_contact_added>/<int:contact_id>/', views.contact_update, name = "contact_update"),

    path('tool_edit_update/<int:tool_id>/', views.tool_edit_update, name = 'tool_edit_update'),
    path('tool_edit/<int:tool_id>/', views.tool_edit, name = "tool_edit"),
    path('tool/<int:category_id>',views.tool, name = 'tool'),
    ]
