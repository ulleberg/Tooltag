from django.apps import AppConfig


class ByggmesteranConfig(AppConfig):
    name = 'byggmesteran'
